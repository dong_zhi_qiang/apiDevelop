
<?php


class MD5_Lite{

  protected $config = array(

        // APPkey
        'accessKey' => '',
        'Key' => '',
    );

 public function __construct() {
        if(DI()->config->get('app.Md5'))
            $this->config = array_merge($this->config, DI()->config->get('app.Md5'));
    }

 public function md5_encode($data){
 	 $config=$this->config;
     if($data==""){
      	$this->setError('data is not empty！');
        return false;	
     }else{ 
     $mcrypt = new PhalApi_Crypt_MultiMcrypt($config['accessKey']);
       $encryptData = $mcrypt->encrypt($data,$config['Key'] ); 
       return $encryptData;
     }
  }
 public function  md5_decode($data){
     $config=$this->config;
     if($data==""){
      	$this->setError('data is not empty！');
        return false;	
     }else{ 
     $mcrypt = new PhalApi_Crypt_MultiMcrypt($config['accessKey']);
        $decryptData = $mcrypt->decrypt($data,$config['Key'] ); 
        return $decryptData;
     }
 }



}