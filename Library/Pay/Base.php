<?php

abstract class Pay_Base {

    protected $config;
    protected $info;
    
    public function __construct($config) {
        $this->config = array_merge($this->config, $config);
    }

    /**
     * 配置检查
     * @return boolean
     */
    public function check() {
        return true;
    }

    /**
     * 验证通过后获取订单信息
     * @return type
     */
    public function getInfo() {
        return $this->info;
    }

    /**
     * 生成订单号
     * 可根据自身的业务需求更改
     */
    public function createOrderNo() {
        $year_code = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        return $year_code[intval(date('Y')) - 2010] .
                strtoupper(dechex(date('m'))) . date('d') .
                substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('d', rand(0, 99));
    }

    /**
     * 建立提交表单
     */
    abstract public function buildRequestForm($vo);

   /**
   *构建字符串
   */
   protected function _buildForm($data, $is_encode=false){
        $arr = empty($data) ? $this->alipay_config : $data;
        $arg = '';
        foreach( $arr as $key => $value ) {
            if($is_encode) {
                $key = urlencode($key);
                $value = urlencode($value);
            }
            $arg .= $key . '=' . $value . '&';
        }
        $arg = substr($arg, 0, strlen($arg)-1); //去掉最后一个&
        //如果存在转义字符，那么去掉转义
        //if(get_magic_quotes_gpc()) {$arg = stripslashes($arg);}
         
        return $arg;

   }

    // /**
    //  * 构造表单
    //  */

    // protected function _buildForm($params, $gateway, $method = 'post', $charset = 'utf-8') {

    //     header("Content-type:text/html;charset={$charset}");
    //     $sHtml = "<form id='paysubmit' name='paysubmit' action='{$gateway}' method='{$method}'>";

    //     foreach ($params as $k => $v) {
    //         $sHtml.= "<input type=\"hidden\" name=\"{$k}\" value=\"{$v}\" />\n";
    //     }

    //     $sHtml = $sHtml . "</form>Loading......";

    //     $sHtml = $sHtml . "<script>document.forms['paysubmit'].submit();</script>";
    //     return $sHtml;
    // }

    /**
     * 支付通知验证
     */
    abstract public function verifyNotify($notify);

    /**
     * 异步通知验证成功返回信息
     */
    public function notifySuccess() {
        return "success";
    }

    /**
     * 异步通知验证失败返回信息
     * @return [type] [description]
     */
    public function notifyError(){
        echo "fail";
    }

    final protected function formatPostkey($post, &$result, $key = '') {
        foreach ($post as $k => $v) {
            $_k = $key ? $key . '[' . $k . ']' : $k;
            if (is_array($v)) {
                $this->formatPostkey($v, $result, $_k);
            } else {
                $result[$_k] = $v;
            }
        }
    }

}
