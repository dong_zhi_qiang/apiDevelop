<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:06
 */
class Domain_Danmaku {

    //插入评论资料
    public function add($data){
        $model = new Model_Danmaku();

        return $model->add($data);
    }

    //查询列表的
    public function getDataByField($field,$arg){
        $model = new Model_Danmaku();

        return $model->getDataByField($field,$arg);
    }

    //查询出最新的
    public function getNewContentList($arg){
        $model = new Model_Danmaku();

        return $model->getNewContentList($arg);
    }

    //统计楼层数
    public function countNumber($arg,$field){
        $model = new Model_Danmaku();

        return $model->countNumber($arg,$field);
    }



}