<?php

/**
 * 活动管理
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:02
 */
class Api_Active extends PhalApi_Api
{
    public function getRules()
    {
        return [
            'add' => [
                'username' => ['name' => 'username', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '直播频道的名称'],
                'channelName' => ['name' => 'channelName', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '直播频道的名称'],
                'channelDescribe' => ['name' => 'channelDescribe', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '直播频道的名称'],
                'activeTime' => ['name' => 'activeTime', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '开始时间'],
                'logo' => ['name' => 'logo', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => 'logo'],
                'outputSourceType' => ['name' => 'outputSourceType', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '选择输出源类型(输出3有RTMP/FLV HLS)'],
                'outputRate' => ['name' => 'outputRate', 'type' => 'array', 'format' => 'explode', 'require' => true,'desc' => '原画：0 标清：10 高清：20::0必传', 'separator' => ','],
                'watermarkId' => ['name' => 'watermarkId', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '是否显示水印'],
                'channelId' => ['name' => 'channelId', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '直播ID'],
                'address' => ['name' => 'address', 'type' => 'array', 'format' => 'explode', 'require' => true,'desc' => '原画：0 标清：10 高清：20::0必传', 'separator' => ','],
//                'address' => ['name' => 'address', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '直播源'],
//                'address2' => ['name' => 'address2', 'type' => 'string','require' => true, 'errorCode' => -1101, 'desc' => '直播源'],
            ],
            'getBaseInfo'=>[
                'page' => ['name' => 'page', 'type' => 'int','require' => true, 'desc' => '分页'],
            ],
            'getChannelId'=>[
                'id' => ['name' => 'id', 'type' => 'int','require' => true, 'desc' => '直播id'],
            ]
        ];
    }

    /**
     * 新增活动
     * @return mixed
     */
    public function add()
    {
        $active = new Domain_Active();

        $data = [
            'username' => $this->username,
            'channelName' => $this->channelName,
            'channelDescribe' => $this->channelDescribe,
            'activeTime' => $this->activeTime,
            'logo' => $this->logo,
            'outputSourceType' => $this->outputSourceType,
            'outputRate' => json_encode($this->outputRate),
            'watermarkId' => $this->watermarkId,
            'channelId' => $this->channelId,
            'downstream_address' => json_encode($this->address)
        ];

        return $active->add($data);
    }

    /**
     * 获取多个基本信息
     * @desc 图片/时间/描述...
     */
    public function getBaseInfo(){
        $active = new Domain_Active();
        $field = 'id,logo,channelName,channelDescribe,activeTime';
        $arg = [
            'isHot' => 0
        ];
        $limit2= ($this->page-1)*8;
        $offset2= 8;

        return [
            'homeData' => $active->getBaseInfo($field,$arg,$limit2,$offset2),
            'count' => $active->countNumber()
        ];
    }

    /**
     * 获取热门直播
     * @return mixed
     */
    public function getHotInfo(){
        $active = new Domain_Active();
        $field = 'id,logo,channelName,channelDescribe,activeTime';
        return $active->getHotInfo($field,['isHot' => 1]);
    }

    /**
     * 根据字段获取数据
     */
    public function getChannelId(){
        $active = new Domain_Active();
        $field = 'channelId';
        $arg = [
            'id' => $this->id
        ];


        return $active->getFiledByData($field,$arg);
    }
}






























