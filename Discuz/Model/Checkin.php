<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:06
 */
class Model_Checkin extends PhalApi_Model_NotORM
{
    //累计签到
    public function total($field,$order,$limit)
    {
        return $this->getORM()
            ->select($field)
            ->order($order)
            ->limit($limit['start'],$limit['end']);
    }


    //累计签到
    public function today($field,$order,$limit)
    {
        return $this->getORM()
            ->select($field)
            ->where('todayrank > 0')
            ->order($order)
            ->limit($limit['start'],$limit['end']);
    }



    protected function getTableName($id)
    {
        return 'zixc_fx_checkin';
    }

}
