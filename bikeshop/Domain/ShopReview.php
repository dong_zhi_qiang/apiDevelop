<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/9
 * Time: 9:46
 */
class Domain_ShopReview {

    public function add($data){
        $model = new Model_ShopReview();

        return $model->add($data);
    }

    public function getList($sid)
    {
        $model = new Model_ShopReview();

        return $model->getList($sid);
    }

}