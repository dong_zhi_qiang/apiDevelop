<?php

/**
*
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/28
 * Time: 10:36
 */
class Domain_Message {
    
    public function getNews($uid){
       $model=new Model_Message;
     
       return  $model->getNews($uid);   
    }

    public function houNews($uid,$touid){
       
       $model=new Model_Message;
     
       return  $model->houNews($uid,$touid);   
    }

   public function inMessage($data){

      $model =new Model_Message();
      
      return  $model->inMessage($data);
    }

    public function inNews($data){
       
       $model =new Model_Message();

       return  $model->inNews($data);
    }
    public function delNew($pmid){
      
        $model =new  Model_Message();
         
        return $model->delNew($pmid); 
    }
}