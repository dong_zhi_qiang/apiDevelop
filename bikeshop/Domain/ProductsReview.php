<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/9
 * Time: 9:46
 */
class Domain_ProductsReview
{

    public function add($data)
    {
        $model = new Model_ProductsReview();

        return $model->add($data);
    }

    public function getList($gid)
    {
        $model = new Model_ProductsReview();

        return $model->getList($gid);
    }

}