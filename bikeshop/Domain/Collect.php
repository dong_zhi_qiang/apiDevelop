<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/18
 * Time: 9:31
 */

class Domain_Collect {

    //添加收藏数据
    public function add($data){
        $model = new Model_Collect();

        return $model->add($data);
    }

    //根据uid读取gid数据
    public function getByUid($uid,$field){
        $model = new Model_Collect();

        return $model->getByUid($uid,$field);
    }
   //根据uid/gid数据
    public function getStatus($uid,$gid){
        $model = new Model_Collect();

        return $model->getStatus($uid,$gid);
    }

    //根据收藏id删除数据
    public function delete($id){
        $model = new Model_Collect();

        return $model->delete($id);
    }

    //获取uid获取id 跟 time
    public function getBYId($id){
        $model = new Model_Collect();

        return $model->getBYId($id);
    }

    //获取uid获取id 跟 time
    public function getCollectData($id,$uid){
        $model = new Model_Collect();

        return $model->getCollectData($id,$uid);
    }



}