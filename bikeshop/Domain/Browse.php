<?php

/**
 *
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 10:36
 */
class Domain_Browse
{

    //
    public function Browse($uid, $start, $end)
    {

        $model = new Model_Browse();

        return $model->getBrowse($uid, $start, $end);
    }


    public function inBrowse($data)
    {

        $model = new Model_Browse();

        return $model->inBrowse($data);
    }

    public function detele($uid)
    {

        $model = new Model_Browse();

        return $model->detele($uid);

    }

    public function getFieldByData($field,$arg)
    {

        $model = new Model_Browse();

        return $model->getFieldByData($field,$arg);

    }


    public function getFieldEdit($arg,$data){
        $model = new Model_Browse();

        return $model->getFieldEdit($arg,$data);
    }




}