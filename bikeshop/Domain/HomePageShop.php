<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/22
 * Time: 14:04
 */
class Domain_HomePageShop {

    //添加门店信息
    public function add($insert){
        $model = new Model_HomePageShop();

        return $model->add($insert);
    }

    //删除门店
    public function delete($id){
        $model = new Model_HomePageShop();

        return $model->delete($id);
    }

    //更新门店信息
    public function edit($update,$arg){
        $model = new Model_HomePageShop();

        return $model->edit($update,$arg);
    }

    //根据uid获取门店列表
    public function getUidList($uid,$field){
        $model = new Model_HomePageShop();

        return $model->getUidList($uid,$field);
    }
  //获取门店名称
      public function getName($id){
        $model = new Model_HomePageShop();

        return $model->getName($id);
    }

    //根据id获取门店列表
    public function getIdList($id,$field){
        $model = new Model_HomePageShop();

        return $model->getIdList($id,$field);
    }

    //获取默认没有筛选列表
    public function getDefaultList(){
        $model = new Model_HomePageShop();

        return $model->getDefaultList();
    }

    //获取id 和 gid
    public function getIDGid(){
        $model = new Model_HomePageShop();

        return $model->getIDGid();
    }


    //获取商店
    public function shop() {
        $model = new Model_HomePageShop();

        return $model->getShop();
    }

    //获取所有详细信息
    public function details($id) {
        $model = new Model_HomePageShop();

        return $model->getDetails($id);
    }

    //根据shopID 获取 到gid
    public function getGid($sid){
        $model = new Model_HomePageShop();

        return $model->getGid($sid);
    }

    //根据门店的id,获取门店banner信息
    public function getBanner($id,$field){
        $model = new Model_HomePageShop();

        return $model->getBanner($id,$field);
    }


    //根据默认页面条件删选内容
    public function screening($data){
        $model = new Model_HomePageShop();

        return $model->screening($data);
    }

    //根据分类后的商店筛选
    public function getGidScreening($order){
        $model = new Model_HomePageShop();

        return $model->getGidScreening($order);
    }

    //计算距离
    public function distance($longitude,$latitude,$longitude2,$latitude2){
        $model = new Model_HomePageShop();

        return $model->distance($longitude,$latitude,$longitude2,$latitude2);
    }

    //获取门店基本信息
    public function getShopBaseInfo($field,$arg){
        $model = new Model_HomePageShop();

        return $model->getShopBaseInfo($field,$arg);
    }

    //修改门店logo
    public function updateLogo($id,$uid,$logo){
        $model = new Model_HomePageShop();

        return $model->updateLogo($id,$uid,$logo);
    }

    //根据sid查询商家name
    public function getBySidTitle($sid){
        $model = new Model_HomePageShop();

        return $model->getBySidTitle($sid);
    }

    //根据sid查询商家gid
    public function getBySidGid($sid){
        $model = new Model_HomePageShop();

        return $model->getBySidGid($sid);
    }


    //根据sid查询商家title address
    public function getBySidAddress($sid){
        $model = new Model_HomePageShop();

        return $model->getBySidAddress($sid);
    }


    //模糊查询门店后期开发,暂时保留
//    private function search($field,$data){
//        $model = new Model_HomePageShop();
//
//        return $model->search($field,$data);
//    }









    //根据商品id上架上架功能
    public function shelves($id,$data){

        $model = new Model_HomePageShop();

        return $model->shelves($id,$data);
    }


    //根据shopID 获取 到did 下架产品
    public function getDid($sid){
        $model = new Model_HomePageShop();

        return $model->getDid($sid);
    }

    //插入did 下架商品id
    public function downShelves($data,$did){
        $model = new Model_HomePageShop();

        return $model->downShelves($data,$did);
    }

    //更新上架商品id值
    public function updateGid($id,$gid){
        $model = new Model_HomePageShop();

        return $model->updateGid($id,$gid);
    }

    //更新下架商品id值
    public function updateDid($id,$did){
        $model = new Model_HomePageShop();

        return $model->updateDid($id,$did);
    }









}