<?php

/**
*
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/24
 * Time: 16:36
 */
class Domain_Login {

    //验证用户信息
    public function user($username) {
       
        $model = new Model_Login();

        return $model->getUser($username);
    }
   
   public function appKey($appKey){
      
      $model =new Model_Login();

      return $model->getAppkey($appKey);

   }

   public function getNum($uid){
        $model =new Model_Login();     
        
        return $model->getNum($uid);
   }

   public function inBaseUser($data){

      $model =new Model_Login();
      
      return  $model->inUser($data);
  }
  //查微信
  public function checkWei($unionid){
        $model =new Model_Login();     
        
        return $model->checkWei($unionid);
   }
   //查 QQ
   public  function checkQq($unionid){
      
        $model =new Model_Login();     
        
        return $model->checkQq($unionid);
   }
  //查UC微信
   public function getWei($unionid){
        $model =new Model_Login();     
        
        return $model->getWei($unionid);
   }
  //插入微信
   public function inWei($data){
        $model =new Model_Login();     
        
        return $model->inWei($data);
   }

}