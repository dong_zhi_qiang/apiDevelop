<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/22
 * Time: 9:36
 */
class Domain_HomePageBanner {

    //获取banner图片
    public function banner() {
        $model = new Model_HomePageBanner();

        return $model->getBanner();
    }
}