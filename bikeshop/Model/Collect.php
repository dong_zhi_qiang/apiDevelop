<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/18
 * Time: 9:31
 */
class Model_Collect extends PhalApi_Model_NotORM
{

    //添加收藏数据
    public function add($data)
    {
        return $this->getORM()
            ->insert($data);
    }

    //根据uid获取数据
    public function getByUid($uid,$field)
    {
        return $this->getORM()
            ->select($field)
            ->where('uid', $uid)
            ->fetchAll();
    }

    //根据uid gid 查询是否收藏
    public function  getStatus($uid,$gid){
         return $this->getORM()
         ->select("id")
         ->where('uid',$uid)->and("gid",$gid)
         ->fetch();
    }

    //获取uid获取id 跟 time
    public function getBYId($id){
        return $this->getORM()
            ->select('id','time')
            ->where('uid', $id)
            ->fetchAll();
    }


    //根据收藏id删除数据
    public function delete($id)
    {
        return $this->getORM()
            ->where('id', $id)
            ->delete();
    }

    public function getCollectData($id,$uid){
        $sql = "SELECT
	a.id,
	a.gid,
	a.time,
	b.title,
	b.image_url,
	b.retail_price,
    a.uid
FROM
	zixc_bikeshop_product_collect AS a
JOIN zixc_bikeshop_goods AS b ON a.gid = b.id
WHERE
	b.id = :id AND a.uid=:uid";

        $param =[
            ':id'=>$id,
            ':uid'=>$uid,
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }


    protected function getTableName($id)
    {
        return 'zixc_bikeshop_product_collect';
    }

}

