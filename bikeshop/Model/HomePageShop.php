<?php

class Model_HomePageShop extends PhalApi_Model_NotORM
{

    //根据uid添加门店
    public function add($insert)
    {
        return $this->getORM()
            ->insert($insert);
    }

    //删除门店
    public function delete($id)
    {
        return $this->getORM()
            ->where('id', $id)
            ->delete();
    }

    //编辑门店
    public function edit($update,$arg)
    {
        return $this->getORM()
            ->where($arg)
            ->update($update);
    }

    //获取商店
    public function getShop()
    {
        return $this->getORM()
            ->select('id', 'star', 'title', 'image_url', 'address')
            ->where('isRecommend', '1')
            ->fetchAll();
    }

   //获取商店
    public function getShopAll()
    {
        return $this->getORM()
            ->select( 'title', 'logo')
            ->where('isRecommend', '1')
            ->fetchAll();
    }
    //根据uid获取门店列表
    public function getUidList($uid, $field)
    {
        return $this->getORM()
            ->select($field)
            ->where('uid', $uid)
            ->fetchAll();
    }

    //根据id获取门店列表
    public function getIdList($id, $field)
    {
        return $this->getORM()
            ->select($field)
            ->where('id', $id)
            ->fetchAll();
    }

    //获取默认没有筛选列表
    public function getDefaultList()
    {
        return $this->getORM()
            ->select('id', 'title', 'brand_type', 'address', 'tel', 'store_size', 'longitude', 'latitude')
            ->order('id DESC')
            ->fetchAll();
    }

    //获取id 和 shelve_id
    public function getIDGid()
    {
        return $this->getORM()
            ->select('id', 'gid')
            ->fetchAll();
    }

    //获取所有详细信息
    public function getDetails($id)
    {
        return $this->getORM()
            ->select('*')
            ->where('id', $id)
            ->fetchAll();
    }

    //获取门店名称
    public function getName($id)
    {
        return $this->getORM()
            ->select('title,uid,address,tel,longitude,latitude')
            ->where('id', $id)
            ->fetch();
    }


    //根据shopID 获取 shelve_id
    public function getGid($sid)
    {
        return $this->getORM()
            ->select('gid,shelve_id')
            ->where('id', $sid)
            ->fetchOne();
    }

    //根据shopID 获取 到downShelve_id
    public function getDid($sid)
    {
        return $this->getORM()
            ->select('downShelve_id')
            ->where('id', $sid)
            ->fetchOne();
    }

    //上架
    public function shelves($id, $data)
    {
        return $this->getORM()
            ->where('id', $id)
            ->update($data);
    }

    //下架
    public function downShelves($data, $did)
    {
        return $this->getORM()
            ->where($data)
            ->update($did);
    }

    //更新shelve_id字段
    public function updateGid($id, $gid)
    {
        return $this->getORM()
            ->where('id', $id)
            ->update($gid);
    }

    //更新gid字段
    public function updateDid($id, $did)
    {
        return $this->getORM()
            ->where('id', $id)
            ->update($did);
    }


    //筛选
    public function screening($data)
    {

        if (empty($data['shop_type'])) {
            $sql = "SELECT id,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop WHERE FIND_IN_SET(:brand_type,brand_type) AND store_size >= :one AND store_size <= :two";
        }

        if (empty($data['brand_type'])) {
            $sql = "SELECT id,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop WHERE shop_type=:shop_type AND store_size >= :one AND store_size <= :two";
        }

        if (empty($data['store_size >= ?'])) {
            $sql = "SELECT id,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop WHERE shop_type=:shop_type AND FIND_IN_SET(:brand_type,brand_type)";
        }

        if (empty($data['brand_type']) && empty($data['store_size >= ?'])) {
            $sql = "SELECT id,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop WHERE shop_type=:shop_type";
        }

        if (empty($data['shop_type']) && empty($data['store_size >= ?'])) {
            $sql = "SELECT id,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop WHERE FIND_IN_SET(:brand_type,brand_type)";
        }

        if (empty($data['shop_type']) && empty($data['brand_type'])) {
            $sql = "SELECT id,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop WHERE  store_size >= :one AND store_size <= :two";
        }

        if (!empty($data['shop_type']) && !empty($data['brand_type']) && !empty($data['store_size >= ?'])) {
            $sql = "SELECT id,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop WHERE shop_type=:shop_type AND FIND_IN_SET(:brand_type,brand_type) AND store_size >= :one AND store_size <= :two";
        }

        @$param = [
            ':shop_type' => $data['shop_type'],
            ':brand_type' => $data['brand_type'],
            ':one' => $data['store_size >= ?'],
            ':two' => $data['store_size <= ?'],
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    //根据分类后的商店筛选
    public function getGidScreening($order)
    {

        $sql = "SELECT id,star,title,address,tel,longitude,latitude FROM zixc_bikeshop_shop ORDER BY $order";

        return DI()->notorm->multi_query->queryAll($sql);
    }

    //根据门店的id,获取门店banner信息
    public function getBanner($id, $field)
    {
        return $this->getORM()
            ->select($field)
            ->where('id', $id)
            ->fetchAll();
    }

    //根据经纬度计算距离
    public function distance($longitude, $latitude, $longitude2, $latitude2)
    {
//        $sql = "SELECT  st_distance (point ($longitude,$latitude),point($longitude2,$latitude2) ) * 111195";
        $sql = "SELECT slc('$longitude','$latitude','$longitude2','$latitude2') from DUAL";
//        SELECT slc('.120.184626.','.30.301459.','.120.187446.','.30.268562.') from DUAL
        $param = [
            ':longitude1' => $longitude,
            ':latitude' => $latitude,
            ':longitude' => $longitude2,
            ':latitud2' => $latitude2,

        ];
        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    //获取门店基本信息
    public function getShopBaseInfo($field,$arg)
    {
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetchOne();
    }

    //根据用户id  商品id修改logo
    public function updateLogo($id, $uid, $logo)
    {
        return $this->getORM()
            ->where([
                'id' => $id,
                'uid' => $uid
            ])
            ->update($logo);
    }

    //根据商家id获取title
    public function getBySidTitle($sid)
    {
        return $this->getORM()
            ->select('id,title')
            ->where([
                'id' => $sid,
            ])
            ->fetchOne();
    }

    //根据商品id获取地址
    public function getBySidAddress($sid)
    {
        return $this->getORM()
            ->select('id,title,address')
            ->where([
                'id' => $sid,
            ])
            ->fetchOne();
    }

    //根据商品id获取地址
    public function getBySidGid($sid)
    {
        return $this->getORM()
            ->select('gid')
            ->where([
                'id' => $sid,
            ])
            ->fetchOne();
    }
  //根据uid统计门店数量
    public function getShopNum($uid){
       return $this->getORM()
                   ->where('uid',$uid)
                   ->count(); 
    }



    //模糊查询门店 后期开发,暂时保留
//    private function search($field, $data)
//    {
//        return $this->getORM()
//            ->select($field)
//            ->where($data)
//            ->fetchAll();
//    }


//    public function shopList($gid){
//        'id','title','brand','type','earnest','retail_price','image_url'
//        $sql = "select a.id,b.uid,b.gid,a.title,a.brand,a.type,a.earnest,a.retail_price,a.image_url from zixc_bikeshop_goods as a left join zixc_bikeshop_shop as b on a.id=b.gid where a.id=:gid";
//
//        $param =[
//            ':gid'=>$gid
//        ];
//
//        return DI()->notorm->multi_query->queryAll($sql, $param);
//
//    }


    protected function getTableName($id)
    {
        return 'zixc_bikeshop_shop';
    }
}
