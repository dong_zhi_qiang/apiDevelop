<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:06
 */

class Model_Authenticate extends PhalApi_Model_NotORM {

    //获取商家信息
    public function addShop($data){
        return $this->getORM()
            ->insert($data);
    }

    //根据字段获取数据
    public function getFieldByData($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetch();
    }



    protected function getTableName($id) {
        return 'zixc_bikeshop_shop_company';
    }

}
