<?php

class Model_Distance extends PhalApi_Model_NotORM {

    public function getList() {
        return $this->getORM()
            ->select('distance')
            ->limit(0,4)
            ->fetchAll();
    }



    protected function getTableName($id) {
        return 'zixc_bikeshop_shop_distance';
    }

}