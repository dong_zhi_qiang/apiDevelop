<?php

class Model_NatureVal extends PhalApi_Model_NotORM {

    //根据n_id 查询属性值
    public function getByIdNatureVal($nid){
        return $this->getORM()
            ->select('*')
            ->where('n_id',$nid)
            ->fetchAll();
    }

    //根据id 查询 和n_id 查询nature值
    public function getByIdUidNatureVal($data){
        return $this->getORM()
            ->select('id,n_id,nature_val')
            ->where($data)
            ->fetchAll();
    }

    //根据nature值 查询id和u_id
    public function getByNature($nature){
        return $this->getORM()
            ->select('id','n_id','nature_val')
            ->where('nature_val',$nature)
            ->fetchAll();
    }

    //根据nature值 查询id和u_id
    public function getByNatureNew($data){
        return $data;
        $sql = "
SELECT
	a.id,
	a.n_id,
	a.nature_val,
	b.sid
FROM
	zixc_bikeshop_goods_nature_val AS a,
	zixc_bikeshop_goods_shelves AS b
WHERE
	b.sid = :sid
AND a.nature_val IN :nature";

        $param = [
            ':nature' =>$data[':nature'],
            ':sid' =>$data['sid']
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

  //根据nature值 n_id
    public function getNid($nature){
        return $this->getORM()
            ->select('n_id')
            ->where('nature_val',$nature)
            ->fetch();
    }
    
    //传入字段值,查询值类似in
    public function getByIdNatureValNew($arg){
        $sql = "SELECT id,nature FROM zixc_bikeshop_goods_nature WHERE find_in_set(id,:id);";

        $param =[
            ':id'=>$arg
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    //新查询方法: 根据n_id 查询属性值
    public function getNatureValByIdNid($nature){
        $sql = "
SELECT
	b.nature_val
FROM
	zixc_bikeshop_goods_nature AS a,
	zixc_bikeshop_goods_nature_val AS b
WHERE
	a.id = b.n_id
AND a.nature = '尺寸'";

        $param = [
          ':nature' =>$nature
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    //新查询方法: 根据字段 查询所有数据
    public function getDataByField($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetchAll();
    }




    protected function getTableName($id) {
        return 'zixc_bikeshop_goods_nature_val';
    }
}
