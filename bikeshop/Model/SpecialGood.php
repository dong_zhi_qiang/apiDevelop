<?php

/*
 *特殊商品
 */
class Model_SpecialGood extends PhalApi_Model_NotORM {

  /*
   *查询
   */
   public function getAll($id,$select){
     
     return $this->getORM()
                 ->select('*')
                 ->where('id',$id)
                 ->fetch();
   }
  /*
   *查询围观人数
   */ 
  public function getNum($id){
     
     return $this->getORM()
                 ->select('number')
                 ->where('id',$id)
                 ->fetch();
   }
 /*
  *根据店铺查询列表
  */
   public function getShopList($shop){
     
     return $this->getORM()
                 ->select('sign,title,price,cost_price,image_url')
                 ->where('shop',$shop)
                 ->order('id DESC')
                 ->fetchAll(); 
 
   }
 /*
  *查询列表
  */
   public function getList($shop){
     
     return $this->getORM()
                 ->select('sign,title,price,cost_price,image_url')
                 ->order('id DESC')
                 ->fetchAll();
 
   } 
  /*
   *添加
   */
 public function  add($data){
    
    return $this->getORM()

                ->insert($data);
   }

  /*
   *编辑
   */  
 public function updated($id,$data){
      
      return $this->getORM()
                  ->where('id',$id)
                  ->updated($data);    
  }

  /*
   *删除
   */
  public function deleteGood($id,$dat){
      
      return $this->getORM()
                  ->where('id',$id)
                  ->delete($data);    
  }

	protected  function getTableName($id){

        return 'zixc_bikeshop_special_good';
	}



}