<?php
 /*
 *查询用户信息
 */

class Model_UserCenter extends PhalApi_Model_NotORM {


    public function getUser ($username) {
        return $this->getORM()
            ->select('uid,username,pic,phone,email')
            //->or('phone',$username)->or('email',$username)->or('username',$username)
            ->where('uid',$username)
            ->fetch();
     } 

 

   public function getPwd($uid){
    return $this->getORM()
             ->select('ucid,password,username,email')
             ->where('uid',$uid)
             ->fetch();
    } 

  public function getName($username){
    return $this->getORM()
             ->select('uid')
             ->where('username',$username)
             ->fetch();
    } 

   public function getPic($uid){
      return $this->getORM()
             ->select('ucid')
             ->where('uid',$uid)
             ->fetch();
   }

   public function getShop($uid){
      $user= DI()->notorm->zixc_bikeshop_shop_company;
      return  $user->select('status')->where('uid',$uid )->fetch();
    }

    public function  getAuthenticate($uid){
       $user= DI()->notorm->zixc_bikeshop_authenticate;
    
       return  $user->select('status')->where('uid',$uid )->fetch();
   }



   public  function upPwd($uid,$password){
     $user= DI()->notorm->zixc_bikeshop_user;  
     $rs = $user->where('uid',$uid)->update(['password'=>$password]);
     return $rs;
   }

   public  function upName($uid,$username){
      $user= DI()->notorm->zixc_bikeshop_user;  
      $rs = $user->where('uid',$uid)->update(['username'=>$username]);
      return $rs;
    }

  public function  upload($uid,$pic){
      $user= DI()->notorm->zixc_bikeshop_user;  
      $rs = $user->where('uid',$uid)->update(['pic'=>$pic]);
      return $rs;
  }

   public function  upPhone($uid,$phone){
      $user= DI()->notorm->zixc_bikeshop_user;  
      $rs = $user->where('uid',$phone)->update(['phone'=>$phone]);
      return $rs;

   }
  
  public function  inAuthenticate($data){
     $user=DI()->notorm->zixc_bikeshop_authenticate;  
       $user->insert($data);
       $id = $user->insert_id();
      return $id; 
  }
  
  public function  Authenticate($uid){
       $user=DI()->notorm->zixc_bikeshop_authenticate;  
        return   $user->select('*')
                    ->where('uid',$uid)
                    ->fetch();     
  }
    public function getDataByUid($uid){
        $user=DI()->notorm->zixc_bikeshop_authenticate;
        return   $user->select('name,number,address')
            ->where('uid',$uid)
            ->fetch();
    }
 //版本升级接口
   public function getEdition(){
     $user=DI()->notorm->zixc_bikeshop_edition;
      return $user->order("id DESC")
                  ->fetch();
   } 
  //检测版本接口
   public function  testEdition($ver){

     $user=DI()->notorm->zixc_bikeshop_edition;

     return $user->where("verCode > ?",$ver)
                 ->fetch(); 
   }
   //更新下载人数
   public function updowloadNUm($id,$data){
     
     $user=DI()->notorm->zixc_bikeshop_edition;
     return $user->where('id',$id)
                 ->update($data);

   }

  /*
  *统计用户量
  */
  public function countUser(){
    
    $user=DI()->notormWei->pre_ucenter_members; 

    return  $user->count();
  }


    protected function getTableName($id) {
        
      return 'zixc_bikeshop_user';
    
    }

}
