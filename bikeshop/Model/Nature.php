<?php

class Model_Nature extends PhalApi_Model_NotORM {


    //类型category id 查询id和分类
    public function getByIdNature($cid){
        return $this->getORM()
            ->select('id,nature')
            ->where('c_id',$cid)
            ->fetchAll();
    }


    //根据nid 查询id和值
    public function getByNIdNature($nid){
        return $this->getORM()
            ->select('id,nature')
            ->where('id',$nid)
            ->fetchAll();
    }

    //根据c_id 查询id和值
    public function getByCIdNature($cid){
        return $this->getORM()
            ->select('id,nature')
            ->where('c_id',$cid)
            ->fetchAll();
    }

    //新查询方法: 根据n_id查询属性名
    public function getByNIdNatureNew($arg){
        $sql = "SELECT n_id,nature_val FROM zixc_bikeshop_goods_nature_val WHERE find_in_set(n_id,:id);";

        $param =[
            ':id'=>$arg
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }





    protected function getTableName($id) {
        return 'zixc_bikeshop_goods_nature';
    }
}
