<?php

class Model_Bank extends PhalApi_Model_NotORM {

    public function getList() {
        return $this->getORM()
            ->select('*')
            ->fetchAll();
    }
    //
   public function getBank(){
     return $this->getORM()
            ->select('id,bank')
            ->fetchAll();
   }
   //查看银行资料
   public function getInfo($bank){
     return $this->getORM()
            ->select('detail')
            ->where('bank',$bank)
            ->fetch();
   }
   //获取银行利率
   public function getRate($id){
   	  return $this->getORM()
            ->select('rate')
            ->where('id',$id)
            ->fetch();
   }

   //获取默认银行利率
   public function getRateOne(){
   	  return $this->getORM()
            ->select('rate')
            ->fetch();
   }

    protected function getTableName($id) {
        return 'zixc_bikeshop_bank';
    }

}