<?php
 /*
 *查询用户信息
 */

class Model_TheOrder extends PhalApi_Model_NotORM {


    public function getOrder ($uid,$status) {
        if($status==3){
         return $this->getORM()
            ->select('shop,shop_id,status,order_id,money,trade_no,type')
            ->where(array_keys($uid)[0],array_values($uid)[0])->and('status >= ?',$status)
            ->order('order_id DESC')
            ->fetchAll(); 
        }else{
        return $this->getORM()
            ->select('shop,shop_id,status,order_id,money,trade_no,type')
            ->where(array_keys($uid)[0],array_values($uid)[0])->and('status',$status)
            ->order('order_id DESC')
            ->fetchAll();
       }
    }
   
   public function orderAll($id){
      return $this->getORM() 
            ->select('trade_no,type,shop,shop_id,uid,pay_name,pay_nper,address,time,money,order_id,status')
            ->where('order_id',$id)
            ->fetch();
   }
  public function getAccept($id){
     return $this->getORM()
            ->select('order_pic')
            ->where('order_id',$id)
            ->fetch();
  }  
  

   public function  inOrder($data){
      $user= DI()->notorm->zixc_bikeshop_order;
      $user->insert($data);
       $id = $user->insert_id();
      return $id; 
   }

  public  function upOrder($id,$data){
     $user= DI()->notorm->zixc_bikeshop_order;  
     $rs = $user->where('pay_num',$id)->update($data);
     return $rs;
   } 

 public  function delOrd($id){

  return $this->getORM()->where('order_id',$id)->delete();
 
 }

 //通过交易号查询订单
 public function toolFee($trade){
    
    return $this->getORM()->where("trade_no",$trade)->sum("money"); 
 }
  //查询全额、还是定金
 public function getType($trade){

    return $this->getORM()
    ->select('pay_name')
    ->where("trade_no",$trade)
    ->fetch();   
 
  }
 //统计交易号订单数量
 public function getCount($trade){
     
     return $this->getORM()
                   ->where("trade_no",$trade)
                   ->count('order_id');

 } 
 //统计订单数量
  public function countOrder(){
    
    return $this->getORM()
                ->count();  
  }
  //查询交易号
  public function getDelivery($oid){
     
      return $this->getORM()
                  ->select('order_id,shop,good_user,delivery,de_status,pay_time')

                  ->where('order_id',$oid)
                  
                  ->fetch(); 
  }
  //根据提货码查询
  public function verifyCode($uid,$code){
     
     return $this->getORM()
                 ->select('order_id,type,pay_name,pay_nper,money,de_status')
                 ->where('delivery',$code)
                 ->and('good_user',$uid)
                 ->fetch();
   }
    protected function getTableName($id) {
        
      return 'zixc_bikeshop_order';
    
    }

}
