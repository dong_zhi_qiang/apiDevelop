<?php

class Model_Range extends PhalApi_Model_NotORM {

    public function getList() {
        return $this->getORM()
            ->select('*')
            ->fetchAll();
    }



    protected function getTableName($id) {
        return 'zixc_bikeshop_goods_price_range';
    }

}