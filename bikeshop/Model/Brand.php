<?php

class Model_Brand extends PhalApi_Model_NotORM {

    public function getList() {
        return $this->getORM()
            ->select('logo','brand')
            ->where('is_show',1)
            ->fetchAll();
    }

  public function getBrand($field,$arg){

  	return $this->getORM()
     	   ->select($field)
     	   ->where($arg)
     	   ->fetchAll();
  }

    protected function getTableName($id) {
        return 'zixc_bikeshop_goods_brand';
    }

}