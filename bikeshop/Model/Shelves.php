<?php

class Model_Shelves extends PhalApi_Model_NotORM {

    public function add($data) {
        return $this->getORM()
            ->insert($data);
    }

    //根据id uid sid nature 查询所有
    public function getByIdSidNatureVal($field,$data){
        return $this->getORM()
            ->select($field)
            ->where($data)
            ->fetchAll();
    }

    //根据id uid sid nature 查询库存量
    public function getByIdSidNumber($field,$data){
        return $this->getORM()
            ->select($field)
            ->where($data)
            ->fetchOne();
    }

    //更新number值
    public function editNumber($arg,$data){
        return $this->getORM()
            ->where($arg)
            ->update($data);
    }

    //根据uid sid 查询上架内容
    public function getByUidSidContent($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetchAll();
    }

    public function editNumberTwo($arg){
        $sql = "UPDATE zixc_bikeshop_goods_shelves SET number  = number - :number WHERE (sid = :sid) AND (gid = :gid) AND (nature_val = :nature_val)";

        $param =[
            ':number'=>$arg['number'],
            ':sid'=>$arg['sid'],
            ':gid'=>$arg['gid'],
            ':nature_val'=>$arg['nature_val'],
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }



    protected function getTableName($id) {
        return 'zixc_bikeshop_goods_shelves';
    }

}