<?php

class Model_HomePageProducts extends PhalApi_Model_NotORM
{

    //根据id获取详细信息
    public function getByIdDetails($id, $field)
    {
        return $this->getORM()
            ->select($field)
            ->where('id', $id)
            ->fetchOne();
    }

    public function screening($data)
    {

//        return $data;
        if (empty($data['category'])) {
            $sql = "SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods limit 0,:page";
        } else {
            $sql = "SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods WHERE category=:category limit 0,:page";
        }

        if (!empty($data['brand'])) {
            if (strpos($sql, 'WHERE')) {
                $sql = substr($sql, 0, strpos($sql, 'limit'));
                $sql = $sql . ' AND brand = :brand';
            } else {
//                SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods limit 0,:page
//                return $sql;
//                SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods limit 0,:page
                $sql = substr($sql, 0, strpos($sql, 'limit'));
                $sql = $sql . ' WHERE brand = :brand limit 0,:page';
//                echo $sql;
//                return $sql;
//                return 5455;
            }
        }

        if (!empty($data['retail_price'])) {
            if (strpos($sql, 'WHERE')) {
                if ($data['retail_price'] == '低到高') {
                    //echo 3;
//                    return 456;
                    $sql = substr($sql, 0, strpos($sql, 'limit'));
                    $sql = $sql . ' ORDER BY retail_price ASC limit 0,:page';
                } else {
                    //  echo 4;
//                    return 123;
                    $sql = substr($sql, 0, strpos($sql, 'limit'));
                    $sql = $sql . ' ORDER BY retail_price DESC limit 0,:page';
//                    return $sql;
                }
            } else {
//                return $sql;
                if ($data['retail_price'] == '低到高') {
                    //   echo 5;
//                    return 123;
                    $sql = substr($sql, 0, strpos($sql, 'limit'));
                    $sql = $sql . ' ORDER BY retail_price ASC limit 0,:page';
//                    return $sql;
                } else {
                    // echo 6;
//                    return 456;
                    $sql = substr($sql, 0, strpos($sql, 'limit'));
                    $sql = $sql . ' ORDER BY retail_price DESC limit 0,:page';
                }
            }
        }

//        //查找sql中的ASC 是否存在，如果在执行
        if ($data['category'] && $data['brand'] && $data['retail_price']) {
            if (strpos($sql, 'ASC')) {
//            $sql = "SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods WHERE category=:category AND brand=':brand'".$sql;
                $sql = "SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods WHERE category=:category AND brand=:brand ORDER BY retail_price ASC limit 0,:page";
            } elseif (strpos($sql, 'DESC')) {
//            $sql = ' ORDER BY retail_price DESC limit 0,:page';
                $sql = "SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods WHERE category=:category AND brand=:brand ORDER BY retail_price DESC limit 0,:page";
            } else {
                $sql = "SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods WHERE category=:category AND brand=:brand limit 0,:page";

            }
        }
//        $sql = "SELECT id,title,brand,type,earnest,retail_price,image_url FROM zixc_bikeshop_goods WHERE category=:catergory AND brand=':brand'".$sql;
        //否则desc


//        return false;


        @$param = [
            ':category' => $data['category'],
            ':brand' => $data['brand'],
            ':retail_price' => $data['retail_price'],
            ':page' => $data['page'],
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);

    }

    //初始都没选择的时候
    public function getDefaultList($page)
    {
        return $this->getORM()
            ->select('id', 'title', 'brand', 'type', 'earnest', 'retail_price', 'image_url')
            ->limit(0, $page)
            ->order('id DESC')
            ->fetchAll();
    }

    //获取首页商品数据
    public function getProducts($page)
    {
        return $this->getORM()
            ->select('id', 'title', '	retail_price', 'image_url')
            ->where('isRecommend', '1')
            ->limit(0, $page)
            ->order('id DESC')
            ->fetchRows();
    }

    //根据gid 查询 数据
    public function getByGid($gid, $field, $page)
    {
        return $this->getORM()
            ->select($field)
            ->where('id', $gid)
            ->limit(0, $page)
            ->fetchRows();
    }

    //模糊查询商品
    public function search($field, $data)
    {
        return $this->getORM()
            ->select($field)
            ->where($data)
            ->fetchAll();
    }

    //根据gid查询信息
    public function earnest($gid)
    {
        return $this->getORM()
            ->select('id,earnest')
            ->where('id', $gid)
            ->fetchAll();
    }

    //根据商品id 查询部分数据
    public function getByIdData($id, $field)
    {
        return $this->getORM()
            ->select($field)
            ->where('id', $id)
            ->fetchOne();
    }

    //统计商品数量
    public function countGood()
    {

        return $this->getORM()
            ->count();

    }

    protected function getTableName($id)
    {
        return 'zixc_bikeshop_goods';
    }




//    public function getDetails($id) {
//        return $this->getORM()
//                ->select('*')
//                ->where('id',$id)
//                ->order('id DESC')
//                ->fetchAll();
//    }


}
