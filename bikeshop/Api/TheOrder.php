<?php

/**
 * 订单管理-
 * Date: 2016/9/30
 * Time: 15:30
 */
class Api_TheOrder extends PhalApi_Api
{

    public function getRules()
    {
        return [
          'addOrder'=>[
               'c_id'            =>['name'=>'c_id','type'=>'array','require'=>true,'format'=>'json','desc'=>"订单数据[60(店铺ID):[price:100,item:'1,2(购物车ID)']]"],
              // 'title'          =>['name'=>'title','type'=>'string','require'=>true,'desc'=>"商品属性/名称/"],
               //'retail_price'   =>['name'=>'retail_price','type'=>'string','require'=>true,'desc'=>"商品单价"],
              // 'order_price'    =>['name'=>'order_price','type'=>'string','require'=>true,'desc'=>"实付款"],
              // 'order_earnest'  =>['name'=>'order_earnest','type'=>'string','desc'=>"定金"],
              'information'    =>['name'=>'information','type'=>'string','desc'=>"用户信息"],
              'uid'            =>['name'=>'uid','type'=>'int','require'=>true,'desc'=>"用户ID"],
              //'shopid'         =>['name'=>'shopid','type'=>'int','require'=>true,'desc'=>"店铺ID"],
             // 'address'        =>['name'=>'address','type'=>'string','require'=>true,'desc'=>"提货地址"],
               'bank'           =>['name'=>'bank','type'=>'string','desc'=>"分期银行"],
              'nper'           =>['name'=>'nper','type'=>'string','desc'=>"分期期数"],
              //'num'            =>['name'=>'num','type'=>'int','require'=>true,'desc'=>"购买商品数"],
           ],
          'getOrder'=>[
               'uid'            =>['name'=>'uid','type'=>'int','require'=>true,'desc'=>"用户ID"], 
               'status'         =>['name'=>'status','type'=>'int','require'=>true,'desc'=>"订单状态"],  
               'type'           =>['name'=>'type','type'=>'int','require'=>true,'desc'=>"1表示用户订单"],  
            ],  
          'OrderAll'=>[
             'oid'=>['name'=>'oid','type'=>"int",'require'=>true,'desc'=>'订单号'],
          ],

          'acceptOrder'=>[
            'oid'=>['name'=>'oid','type'=>"int",'require'=>true,'desc'=>'订单号'],
            'file'=>[
                   'name' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'受理图片'      
                      ],
          ],
         'delOrder'=>[
              'oid'=>['name'=>'oid','type'=>"int",'require'=>true,'desc'=>'订单号'],
         ], 
         'getBank'=>[
             'price'=>['name'=>'price','type'=>"string",'require'=>true,'desc'=>'订单金额'],
             'bank' =>['name'=>'bank','type'=>"string",'desc'=>'银行ID'], 
         ],
         'getPay'=>[
            'order'=>['name'=>'order','type'=>"string",'require'=>true,'desc'=>'订单号'],
          ],
        'getBankInfo'=>[
             'order'=>['name'=>'order','type'=>"string",'require'=>true,'desc'=>'订单号'],
          ],
          'sendInfo'=>[
            'oid'=>['name'=>'oid','type'=>"string",'require'=>true,'desc'=>'订单号'],
            'file'=>[
                   'name' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'受理图片'      
                      ],
          ], 
          'chaInfo'=>[
             'oid'=>['name'=>'oid','type'=>"string",'require'=>true,'desc'=>'订单号'],
          ],
           'noInfoOrder'=>[
             'oid'=>['name'=>'oid','type'=>"string",'require'=>true,'desc'=>'订单号'],
          ], 
          'delivery'   =>[
            'oid'=>['name'=>'oid','type'=>"string",'require'=>true,'desc'=>'订单号'],
          ],
          'verifyCode' =>[
              'uid'        =>['name'=>'uid','type'=>'int','require'=>true,'desc'=>"用户ID"], 
              'code'        =>['name'=>'code','type'=>'string','require'=>true,'desc'=>"提货码"], 
          ],
        ];
    }
   /**
    *银行
    *@desc 银行
    *@return string bank 银行
    */
   public function bank(){

     $domain=new Domain_TheOrder(); 
     
     $data=$domain->getBank();
     
     return $data; 
   }


   /**
    *银行分期
    *@desc 银行分期
    *@return  string periods  期数
    *@return  string amount  每期还钱金额
    *@return  string result  每期钱数 
    */
    public  function getBank(){

      $domain=new Domain_TheOrder();
      $bank =$this->bank;
      $price=$this->price;
      $data=array();
      if($bank){
        $rate=$domain->getRate($bank);       
      }else{
        $rate=$domain->getRateOne();
      }  
      $rate_t=json_decode($rate['rate']);
      foreach ($rate_t as $k) {
         $rate_r=explode("-",$k);
         $interest=round(($price*$rate_r[1])/$rate_r[0],2);
         $money=round(($price/$rate_r[0]),2)+$interest;
         $date=array(
           'result' =>$money.'',
           'periods'=>$rate_r[0].'期',
           'amount' =>$money.'(含每期手续费'.$interest.')',
          );
         $data[]=$date;
       } 
      return $data;
    }
  /**
   *银行信息
   *@desc 银行资料
   *@return  string bank  贷款银行
   *@return  string nper  银行分期
   *@return  string detail  银行所需资料 
   */
   public function getBankInfo(){
     $domain=new Domain_TheOrder();
     $order=$this->order;
     $order_id=json_decode($order);
     if(is_int($order_id)){ 
        $oid=$order;
      }else{
       foreach ($order_id as $k) {
        $oid=$k;
       }
     }
        $order_all=$domain->orderAll($oid);
        $bank_info=$domain->getInfo($order_all['pay_name']);
        $date['bank']=$order_all['pay_name'];
        $date['nper']=$order_all['pay_nper'];    
        $date['detail']=$bank_info['detail'];

        return $date;
   }
    /**
     *生成订单
     * @desc 订单生成
     * @return int code 操作码，201表示提交成功，  401提交失败
     * @return string msg 操作码  提交成功  提交失败
     *
     */
    public function addOrder(){
         $rs=array();  
         $domain=new Domain_TheOrder();
         $domin=new Domain_ShopCart();
         $dmin=new Domain_HomePageShop(); 
         $shop=array_keys($this->c_id);
         $pay_num=DI()->helper->getOrder(0);//支付号
         $i=0;
         foreach ($this->c_id as $k) {
            $user=$dmin->getName($shop[$i]);
            $data=[
           'pay_num'      =>$pay_num,
           'order_id'     =>DI()->helper->getOrder(1),
           'good_user'    =>$user['uid'],
           'shop'         =>$shop[$i],   
           'time'         =>date("Y-m-d H:i:s"),
           'money'        =>$k['price'],
           'uid'          =>$this->uid,
           'shop_id'      =>$k['item'],
           'address'      =>$user['address'],
           'pay_name'     =>$this->bank,
           'pay_nper'     =>$this->nper,
         ]; 
         foreach (explode(",",$k['item'] ) as $k) {
           $domin->upDisplay($k,$datt=['isDisplay'=>0]); 
         }
           $rt=$domain->inOrder($data);
           $i++;
         }  
        if($rt){
            $rs['code']=201;
            $rs['msg']='提交成功';
            $rs['data']=$pay_num;
            return $rs;  
          }else{
            $rs['code']=401;
            $rs['msg']="提交失败";
            $rs['data']=$pay_num;
            return $rs;
          }
       }
  /**
  *支付成功查询
  *@desc  支付成功查询 
  *@return string type  支付方式
  *@return string  bank  分期银行（全额付时为空）
  *@return folat money  支付金额
  */
  public function getPay(){
     $domain=new Domain_TheOrder();
     $order=$this->order;
     $order_id=json_decode($order);
     $money=0;
     foreach ($order_id as $k) {
        $order_all=$domain->orderAll($k);
        $domain->upOrder($k,['delivery'=>substr(time(),0,8).rand(0000,9999),'pay_time'=>date("Y-m-d")]); //生成提货码
        $money+=$order_all['money'];
        if($order_all['type']=="Aliwap"){
          $date['type']='支付宝';
        }else{
          $date['type']='微信';
         }
        $date['bank']=$order_all['pay_name'];
       } 
       $date['money']=$money;
       return $date; 
  } 
  /**
  *订单查询
  *@desc 订单查询
  *@return sting title  商品属性/分类/名称
  *@return sting order_price 实付款
  *@return sting retail_price 商品单价
  *@return int    num  商品数量
  *@return sting time 时间
  *@return sting  order_id 订单ID
  *@return sting  status   订单状态 -1 已退款 0未付款 1待受理 2受理中  3受理完成 4 受理失败 5(商家是未收款 用户是受理成功)
  *@return sting  image_url 商品图片 
  */  
    public function  getOrder(){
      
      $domain=new Domain_TheOrder();
      $domin=new Domain_ShopCart();
      $dmin=new Domain_HomePageShop(); 
       if($this->type==1){
        $uid['uid']=$this->uid;
       }else{
        $uid['good_user']=$this->uid;
       }
      $re=$domain->Order($uid,$this->status);

      $date=array();
      foreach ($re as $k) {
        $data=array();
        $num=0;
        $shot=$dmin->getName($k['shop']);
        $datt['shop']=$shot['title'];
        $shop=explode(",",$k['shop_id']);
          foreach ($shop as $v){
            $tet=$domin->getall($v);
            $datn=array();
            if($tet){
                 $num+=$tet['number'];              
            foreach (json_decode($tet['nature_val']) as $ke) {
                $n_id=$domain->getNid(['id'=>$ke]);
                if($n_id){
                $nature=$domain->getByNIdNature($n_id[0]['n_id']);
                $datn[]=$nature[0]['nature'].":".$n_id[0]['nature_val']; 
                 } 
              }           
            $tet['nature_val']=implode('/',$datn);           
            $data[]=$tet;
             }

          }
         $datt['trade_no']=$k['trade_no'];
         $datt['type']    =$K['type'];
         $datt['money'] =$k['money'];
         $datt['status']=$k['status'];
         $datt['order_id']=$k['order_id'];
         $datt['number']=$num;
         $datt['good']=$data;
         $date[]=$datt;  
       } 
      return $date;
    }
 /**
 *订单详情
 *@desc 订单详情
  *@return sting title  商品属性/分类/名称
  *@return sting order_price 实付款
  *@return sting retail_price 商品单价
  *@return int    num  商品数量
  *@return sting time 时间
  *@return sting  order_id 订单ID
  *@return sting  status   订单状态
  *@return sting  image_url  商品图片
  *@return sting  order_earnest 订金
  *@return sting  information 认证信息
  *@return sting  address  订单信息
  *@return sting  pay_neme 分期银行
  *@return sting  pay_nper 订单状态  
 */ 
    public function OrderAll(){
     
      $domain=new Domain_TheOrder();
      $domin=new Domain_ShopCart();
      $dmin=new Domain_HomePageShop();
      $autu=new Domain_UserCenter(); 
      $re=$domain->OrderAll($this->oid); 
      $data=array();
      $shot=$dmin->getName($re['shop']);
      $shop=explode(",",$re['shop_id']);
          foreach ($shop as $v){
            $tet=$domin->getall($v);
            $datn=array();
            foreach (json_decode($tet['nature_val']) as $ke) {
                $n_id=$domain->getNid(['id'=>$ke]);
                if($n_id){
                $nature=$domain->getByNIdNature($n_id[0]['n_id']);
                $datn[]=$nature[0]['nature'].":".$n_id[0]['nature_val']; 
                 } 
              }
            $tet['nature_val']=implode('/',$datn);  
            $data[]=$tet;
          } 
      $autu1=$autu->Authenticate($re['uid']);
      $datt['information']=['name'=>$autu1['name'],'tel'=>$autu1['tel'],'address'=>$autu1['address']];
      $datt['good']=$data;
      $date= array_merge($re,$datt);
      $date['shop']=$shot['title'];  
      return $date;  

    }
 /**
 *用户上传资料
 *@desc 用户上传资料
 * @return int code 操作码，201表示提交成功，  401提交失败
 * @return string msg 操作码  提交成功  提交失败
 */
  public function sendInfo(){
      $domain=new Domain_TheOrder();
        $file=$_FILES['file'];
        $name=array();
        $i=0;
        //DI()->logger->log('payError','File', $file);
       foreach ($file['tmp_name'] as $k) {
          $path['type']=$file['type'][$i]; 
          $dst = DI()->helper->getFileName('order',  $path); //获取logo的上传路径      
          $file1 = DI()->cosSdk->uploadCos('bikeshop', $k, $dst);
          $fileScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $file1['data']['source_url']);
          $name[]= $fileScore;
        } 
       $pic=implode(",",$name);
       $data=[
         'order_pic'=>$pic,    
       ];
       $order=json_decode($this->oid);
       foreach ($order as $k) {
        $re=$domain->upOrder($k,$data); 
       }
      if($re){
            $rs['code']=201;
            $rs['msg']="提交成功";
            return $rs;  
          }else{
            $rs['code']=401;
            $rs['msg']="提交失败";
            return $rs;
          }  
    }
  /**
   *检测用户是否上传资料
   *@desc检测用户是否上传资料
   *@return boolen  
   */
  public function chaInfo(){
     $domain=new Domain_TheOrder();
    $re=$domain->getAccept($this->oid);
    if($re['order_pic']){
      return true;
    }else{
      return false;
    }
  }
 /**
 *不提交资料的受理订单
 *@desc 受理订单
 * @return int code 操作码，201表示提交成功，  401提交失败
 * @return string msg 操作码  受理成功  受理失败
 */
 public function noInfoOrder(){
     $domain=new Domain_TheOrder();
      $data=[
          'status'=>2,
       ];
      if($domain->upOrder($this->oid,$data)){
            $rs['code']=201; 
            $rs['msg']="受理成功";
            return $rs;  
          }else{
            $rs['code']=401;
            $rs['msg']="受理失败";
            return $rs;
          }   
 } 
 /**
 *受理订单
 *@desc 受理订单
 * @return int code 操作码，201表示提交成功，  401提交失败
 * @return string msg 操作码  提交成功  提交失败
 */
   public function  acceptOrder(){
        $domain=new Domain_TheOrder();
        $file=$_FILES['file'];
        $name=array();
        $i=0;
       // DI()->logger->log('payError','File', $file);
       foreach ($file['tmp_name'] as $k) {
          $path['type']=$file['type'][$i]; 
          $dst = DI()->helper->getFileName('order',  $path); //获取logo的上传路径      
          $file1 = DI()->cosSdk->uploadCos('bikeshop', $k, $dst);
          $fileScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $file1['data']['source_url']);
          $name[]= $fileScore;
        } 
       $pic=implode(",",$name);
       $data=[
          'status'=>2,
          'order_pic'=>$pic,
       ];
        if($domain->upOrder($this->oid,$data)){
            $rs['code']=201;
            $rs['msg']="提交成功";
            return $rs;  
          }else{
            $rs['code']=401;
            $rs['msg']="提交失败";
            return $rs;
          }  
      }
 /**
  *取消订单
  *@desc 取消订单
  * @return int code 操作码，201表示取消成功，  401取消失败
  * @return string msg 操作码  取消成功  取消失败
  */
  public function  delOrder(){

     $domain=new Domain_TheOrder();  
     
     if($domain->delOrd($this->oid)){
            $rs['code']=201;
            $rs['msg']="取消成功";
            return $rs;  
          }else{
            $rs['code']=401;
            $rs['msg']="取消失败";
            return $rs;
          } 
   }
  /**
  *电子凭证接口（2.0）
  *@desc电子凭证接口
  *@return string  oid 订单id
  *@return string  shopid 商家id用于提货码验证
  *@return string  delivery 提货码
  *@return string  status 0表示还没使用 1已使用 -1已失效
  *@return string  title 店铺名称
  *@return string  tel 联系电话
  *@return string  address 地址
  *@return string  longitude 经度
  *@return string  latitude  维度
  *@return string  start_time~end_time 有效期
  */  
  public function delivery(){

    $domain=new Domain_TheOrder();
    $dmin=new Domain_HomePageShop(); 
    $re=$domain->getDelivery($this->oid);
    $shot=$dmin->getName($re['shop']);
    $data=array(
      "oid"      =>$this->oid,
      "shopid"   =>$re['good_user'],
      'delivery' =>$re['delivery'],
      'status'   =>$re['de_status'],
      'start_time'=>$re['pay_time'],
      'end_time' =>date("Y-m-d",strtotime($re['pay_time'])+1296000),
      'title'    =>$shot['title'],
      'tel'      =>$shot['tel'],
      'address'  =>$shot['address'],
      'longitude'=>$shot['longitude'],
      'latitude' =>$shot['latitude'],
      );
    return $data; 
   }
  /**
   *密码验券（2.0）
   *@desc 商家验券
   * @return int code 操作码，201验证成功，  402验证失败 403 已使用  405已失效
   * @return string type 支付方式，
   * @return string way 支付类型，
   * @return string money 实付款
   * @return string order_id 订单号，
   */
  public function verifyCode(){

     $domain=new Domain_TheOrder();
     $re=$domain->verifyCode($this->uid,$this->code);
     if($re){
         if($re['de_status']==1){
             $rs['code']=403;
             $rs['msg']="已使用";
             return $rs;
             exit; 
         }elseif ($re['de_status']==-1) {
             $rs['code']=403;
             $rs['msg']="已失效";
             return $rs;
             exit;  
         }
         $domain->upOrder($re['order_id'],['de_status'=>1]);
        if($re['type']=="Aliwap"){
          $date['type']='支付宝';
         }else{
          $date['type']='微信';
         }
        if($re['pay_name']){
           $date['way']='分期付款';
        }else{
          $date['way']='全额付款';
        }
        $date['order_id']=$re['order_id'];          
        $date['money' ] =$re['money']; 
        $rs=[
           'code'=>201,
           'msg' =>$date, 
        ];
       return $rs; 
     }else{
        $rs['code']=402;
        $rs['msg']="验证失败";
        return $rs;
     }
  }
}





































































