<?php
/**
 * 首页管理
 * Date: 2016/9/22
 * Time: 9:30
 */

class Api_HomePage extends PhalApi_Api {

    public function getRules() {
        return [
            'homeData'=>[
                  'page' => ['name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '页码'],
            ]
        ];
    }


    /**
     * 获取主页信息
     * @desc banner,热门商品,热门门店,品牌推荐
     * @return array banner     轮播图
     * @return array products   热门商品列表
     * @return array shop       热门门店列表
     * @return array brand      品牌推荐列表
     */
    public function homeData() {
        $banner = new Domain_HomePageBanner();
        $products = new Domain_HomePageProducts();
        $shop = new Domain_HomePageShop();
        $brand=new  Domain_Brand();
        $brandData = $banner->banner();
        foreach($brandData as $k=>$v){
            $title[] = $products->getByIdDetails($v['url'],'title');
            $brandData[$k]['title'] = $title[$k]['title'];
        }

        $arg = [
            'is_show' => 1
        ];

        return [
            'banner' => $brandData,
            'products' => $products->products($this->page*4),
            'shop' => $shop->shop(),
            'brand'=>$brand->getBrand('logo,brand,url',$arg),
        ];
    }


}





































































