<?php

/**
 * 热门搜索
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:02
 */
class Api_HotSearch extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'search' => [
                'title' => ['name' => 'title', 'type' => 'string', 'require' => true, 'errorCode' => -1304, 'desc' => '商品标题'],
            ],
        ];
    }


    /**
     * 热门搜索列表
     * @desc 返回商品id,名称
     * @return int gid 商品id
     * @return string name 商品名称
     */
    public function getList()
    {
        $hotSearch = new Domain_HotSearch();
        $arg = [
            'isHot' => 1
        ];

        return $hotSearch->getDataByField('gid,name', $arg);
    }

    /**
     * 模糊搜索门店
     * @desc 根据title模糊搜索商品
     * @return boolean $rs 列表数据,false表示查询不到
     */
    public function search()
    {
        $products = new Domain_HomePageProducts();

        $field = 'id,title';

        $data = [
            'title LIKE ?' => "%$this->title%",
        ];

        //模糊搜索门店
        $rs = $products->search($field, $data);

        return $rs ? $rs : false;
    }
}


