<?php

/**
 * 登录操作-
 * Date: 2016/9/22
 * Time: 15:33
 */
class Api_Login extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'login' => [
                'username' => ['name' => 'username', 'type' => 'string', 'require' => true, 'desc' => '登录帐号'],
                'password' => ['name' => 'password', 'type' => 'string', 'require' => true, 'desc' => '用户密码'],

            ],
            'OtherLogin'=>[
                'pic'      => ['name' => 'pic', 'type' => 'string', 'require' => true, 'desc' => '第三方用户图像'],
                //'access_token'   => ['name' => 'appkey', 'type' => 'string', 'require' => true, 'desc' => '第三方的key'],
                'username' => ['name' => 'username', 'type' => 'string', 'require' => true, 'desc' => '用户昵称'],
                'unionid'  => ['name' => 'unionid', 'type' => 'string', 'require' => true, 'desc' => '微信/QQ识别ID'],  
                'type'   => array('name' => 'type', 'type' =>'enum', 'require' => true, 'range' => array('qq', 'wechat'), 'desc' => '引擎类型，比如qq'),
            ],
        ];
    }


    /**
     *登录验证
     * @desc 用户登录
     * @return int code 操作码，203表示登录成功，  407用户不存在/密码错误
     * @return string msg 操作码  用户不存在,或者被删除     密码错误
     *
     */
    public function login()
    {
        $rs = array('code' => 0, 'msg' => '');
        $username = $this->username;
        $password = $this->password;

        $domain = new Domain_Login();
        $info = $domain->user($username);
        if ($info) {
            if ($info['password'] == DI()->md5->md5_encode($password)) {   
               // $num=$domain->getNum($info['uid']); 
                // $data=array(
                //   'uid'=>$info['uid'],
                //   'num'=>intval($num,10 ),
                //   'username'=>$username,
                //     );
                $rs['code'] = 203;
                $rs['msg'] = T('登陆成功');
                $rs['data']=$info['uid'];
                return $rs;
            } else {
                $rs['code'] = 407;
                $rs['msg'] = T('密码错误');
                $rs['data']='0';
                return $rs;
            }

        } else {
            $re = DI()->ucenter->uc_login($username, $password);
            if (is_array($re)) { 
               $re= $domain->inBaseUser($re);
               // $data=array(
               //    'uid'=>$re,
               //    'num'=>0,
               //    'username'=>$username,
               //      );
                $rs['code'] = 201;
                $rs['msg'] = T('登录成功');
                $rs['data']=$re;
                return $rs;
            } else {
                $rs['code'] = 407;
                $rs['msg'] = T($re);
                $rs['data']='0';
                return $rs;
            }
        }
    }
  
  /**
     *登录验证
     * @desc 第三方登录微信登录
     * @return int code 操作码，203 表示登录成功，  407登陆失败
     * @return string msg 操作码  407登录失败
     *
     */


    public function OtherLogin(){

       $domain = new Domain_Login();
       $domin = new Domain_UserCenter();
       if($this->type=='qq'){
         $unionid=$this->getNionid($this->unionid); 
       }else{
         $unionid=$this->unionid;
       }
       $uid=$domain->checkWei($unionid);      
      if($uid){
         $rs['code'] = 203;
         $rs['msg'] = T('登录成功');
         $rs['data']=$uid['uid'];
         return $rs;   
       }else{
          if($this->type=='qq'){
             $weichat=$domain->checkQq($unionid);             
          }else{
            $weichat=$domain->getWei($unionid);           
           }

             if($weichat){
                $roe=DI()->ucenter->uc_get_user($weichat['uid'],array(1));
                $data=array(
                   'username'=>$roe[1],
                   'email'   =>$roe[2],
                   'ucid'    =>$weichat['uid'],
                   'pic'     =>DI()->ucenter->uc_image_url($weichat['uid'],null), 
                    );
                  $rt=$domain->inBaseUser($data);   
                return  $this->zhuwei($this->username,$rt,$unionid);
              }else{
                    /*检测用户名*/
                    $check_name=DI()->ucenter->uc_user_checkname($this->username);
                    if($check_name==1){
                       $username1=$this->username;
                    }elseif($check_name==-3){
                     $username1=$this->username."JQ".rand(100,999); 
                   }else{            
                     $username1='jq_'.rand('10000000','99999999').'go';
                    }
                   $email='jq'.rand('100000','999999')."@null.com";
                   $re = DI()->ucenter->register($username1,null, $email); 
                   if (is_int($re)) { 
                    $data=[
                    'username'=>$username1,
                    'ucid'    =>$re,
                    'email'   =>$email,
                    'pic'     =>$this->pic, 
                   ]; 
                  $rt=$domain->inBaseUser($data);
                 return $this->zhuwei($this->username,$rt,$unionid);
               }else{
                $rs['code'] = 407;
                $rs['msg'] = T($re);
                $rs['data']='0';
                return $rs;
               }    
             } 
         } 
      }
  
  /*
  *微信注册
  */
     private function zhuwei($username,$uid,$unionid){ 
       
        $domain = new Domain_Login();
        $date=array(
            'weixinname'=>$username,
            'uid'    =>$uid,
            'unionid'=>$unionid,  
            );       
           $domain->inWei($date);//插入微信登录凭证
           $rs['code'] = 203;
           $rs['msg'] = T('登录成功');
           $rs['data']=$uid;
           return $rs;  
     } 
  /*
  *获取QQ统一unionid
  */
   private function getNionid($access_token){
     
     $url="https://graph.qq.com/oauth2.0/me?access_token=".$access_token."&unionid=1";
      
     $curl = curl_init();

      // 设置你需要抓取的URL
      curl_setopt($curl, CURLOPT_URL, $url);

      // 设置header
      curl_setopt($curl, CURLOPT_HEADER, 0);

      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//https这个是重点。

      // 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

      // 运行cURL，请求网页
      $data = curl_exec($curl);

      // 关闭URL请求
      curl_close($curl); 
      
      preg_match_all("/(?:\()(.*)(?:\))/i",$data,$str_ary);//用正则取出()里的josn串
      
      $date=json_decode($str_ary[1][0]);

      return $date->unionid; 

   }
}





































































