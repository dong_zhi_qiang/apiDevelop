<?php

/**
 * 意见反馈
 * Date: 2017/1/6
 * Time: 16:49
 */
class Api_Error extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'add' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id'],
                'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true, 'desc' => '商品id'],
                'phone' => ['name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '联系号码'],
                'content' => ['name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '反馈内容'],
            ],
        ];
    }


    /**
     * 商品纠错反馈
     * @desc 商品纠错反馈
     * @return boolean rs true/false
     */
    public function add(){
        $idea = new Domain_Error();

        $data = [
            'uid' => $this->uid,
            'gid' => $this->gid,
            'phone' => $this->phone,
            'content' => $this->content,
            'date' => date('Y-m-d H:i:s'),
            'isError' => 1,
        ];

        return $idea->add($data);
    }




}


