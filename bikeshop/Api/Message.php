<?php

/**
 * 消息管理 -
 * Date: 2016/9/28
 * Time: 9:33
 */
class Api_Message extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'feedBack' => [
                'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
                'phone'=> ['name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号'],
                'content'=> ['name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '反馈信息'],
            ],
           'news'=>[
                'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
                'page'=>['name'=>'page','type'=>'int','require'=>true,'desc'=>'当前页数' ],
                'pagesize'=>['name'=>'pagesize','type'=>'int','require'=>true,'desc'=>'每页显示条数' ]
           ],
           'sedNews'=>[
               'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
               'msgto'=>['name'=>'msgto','type'=>'int','require'=>true,'desc'=>'收件人ID' ],
               'message'=>['name'=>'message','type'=>'string','require'=>true,'desc'=>'消息内容'],
            ], 
           'getNews'=>[
               'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
               'touid'=>['name'=>'touid','type'=>'int','require'=>true,'desc'=>'发送消息用户ID' ],
               'page'=>['name'=>'page','type'=>'int','require'=>true,'desc'=>'当前页数' ],
              'pagesize'=>['name'=>'pagesize','type'=>'int','require'=>true,'desc'=>'每页显示条数' ]
           ],
          'newView'=>[
              'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
              'pmid'=>['name'=>'pmid','type'=>'int','require'=>true,'desc'=>'消息ID' ],
          ],
          'delNew'=>[
              'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
              'pmid'=>['name'=>'pmid','type'=>'int','require'=>true,'desc'=>'消息ID' ],
           ],
        ];
    }


    /**
     * 意见反馈
     * @desc 意见反馈
     * @return int code 操作码，211表示反馈成功，  411反馈失败
     * @return string msg 操作码  反馈成功，    反馈失败
     *
     */
    public function feedBack()
    {  
       header("Access-Control-Allow-Origin:*");
     /*星号表示所有的域都可以接受，*/
      header("Access-Control-Allow-Methods:GET,POST");
       $data=[
         'uid'    =>$this->uid,
         'content'=>$this->content,
         'phone'  =>$this->phone,     
       ]; 
       $rs=array();
      $domain=new Domain_Message;
       if($domain->inMessage($data)){
          $rs['code']=211;
          $rs['msg']="反馈成功";
          return $rs;
       }else{
          $rs['code']=211;
          $rs['msg']="反馈失败";
          return $rs;  
       }
    }
  /**
     * 消息中心
     * @desc 消息中心
     * @return    integer ['plid']  会话 ID
     * @return    int ['uid'] 当前用户 ID
     * @return bool ['isnew']  是否未读标记
     * @return int ['pmnum'] 该会话相对于当前用户的消息数量
     * @return int ['lastupdate']  当前用户的最后更新时间
     * @return int ['lastdateline']  会话最后更新时间
     * @return int ['authorid']  会话发起者 ID
     * @return int ['pmtype']  会话类别 1: 私人消息, 2: 群聊消息
     * @return string ['subject']  标题
     * @return int ['members'] 会话参与人数
     * @return int ['touid'] pmtype参数为1时,该返回值表示对方用户 ID
     * @return string ['tousername'] pmtype参数为1时,该返回值表示对方用户名
     * @return int ['founddateline'] 会话发起时间
     * @return int ['lastauthorid']  会话最后一条的发送人 ID
     * @return string ['lastauthor'] 会话最后一条的发送人用户名
     * @return string ['lastsummary']  会话最后一条的发送的内容截取
     * @return int ['pmid'][兼容]  消息 ID
     * @return int ['msgfromid'][兼容] 发件人用户 ID
     * @return string ['msgfrom'][兼容]  发件人用户名
     * @return int ['msgtoid'][兼容] 收件人用户 ID
     * @return int ['new'][兼容] 1:未读短消息0:已读短消息
     * @return string ['message'][兼容]  内容
     * @return int ['dateline'][兼容]  发送时间的时间戳
     * @return int ['daterange'][兼容] 1:今天2:昨天3:前天4:上周5:更早
     * @return int  count   信息总数  
     * @return int  count   信息总数 
     *
     */
    public function news(){
       $domain=new Domain_Message;
       $re=$domain->getNews($this->uid);
       $pade=[$this->page,$this->pagesize];
       $data= DI()->ucenter->uc_pm_list($re['ucid'],$pade);
       return $data;
      }   
  
  /**
     * 消息中心
     * @desc 发送消息
     * @return int  code  212 发送成功  412 发送失败
     * @return string msg   发送成功   发送失败
     *
     */
    public function sedNews(){
       $domain=new Domain_Message;
       $re=$domain->getNews($this->uid); 
       $rt= DI()->ucenter->uc_pm_send($re['ucid'],$this->msgto,"消息发送",$this->message,[1]);
       if($rt>0){
          $data=[
             'uid'=>$this->uid,
             'ucid'=>$re['ucid'],
             'mid'=>$this->msgto,
             'pmids'=>$rt,
          ];
          $domain->inNews($data);
          $rs['code']=212;
          $rs['msg']="发送成功";
          return $rs;
       }else{
          $rs['code']=412;
          $rs['msg']="发送失败";
          return $rs;
       }

    }
  /**
  *消息中心
  *@desc 获取信息
     * @return    integer ['plid']  会话 ID
     * @return    int ['uid'] 当前用户 ID
     * @return bool ['isnew']  是否未读标记
     * @return int ['pmnum'] 该会话相对于当前用户的消息数量
     * @return int ['lastupdate']  当前用户的最后更新时间
     * @return int ['lastdateline']  会话最后更新时间
     * @return int ['authorid']  会话发起者 ID
     * @return int ['pmtype']  会话类别 1: 私人消息, 2: 群聊消息
     * @return string ['subject']  标题
     * @return int ['members'] 会话参与人数
     * @return int ['touid'] pmtype参数为1时,该返回值表示对方用户 ID
     * @return string ['tousername'] pmtype参数为1时,该返回值表示对方用户名
     * @return int ['founddateline'] 会话发起时间
     * @return int ['lastauthorid']  会话最后一条的发送人 ID
     * @return string ['lastauthor'] 会话最后一条的发送人用户名
     * @return string ['lastsummary']  会话最后一条的发送的内容截取
     * @return int ['pmid'][兼容]  消息 ID
     * @return int ['msgfromid'][兼容] 发件人用户 ID
     * @return string ['msgfrom'][兼容]  发件人用户名
     * @return int ['msgtoid'][兼容] 收件人用户 ID
     * @return int ['new'][兼容] 1:未读短消息0:已读短消息
     * @return string ['message'][兼容]  内容
     * @return int ['dateline'][兼容]  发送时间的时间戳
     * @return int ['daterange'][兼容] 1:今天2:昨天3:前天4:上周5:更早
     * @return int  count   信息总数   
  */
     public function getNews(){
       $domain=new Domain_Message;
       $uid=$this->uid;
       $touid=$this->touid;
       $re=$domain->houNews($uid,$touid);   
       $data= DI()->ucenter->uc_pm_view($re['ucid'],$re['pmids'],[$touid,1,$this->page,$this->pagesize]);
       return $data;
     }
    /**
    *消息中心
    *@desc 获取单条信息
    *@return int ['pmid']  消息 ID
    *@return int ['plid']  所属会话 ID
    *@return int ['authorid']  消息发起者 ID
    *@return string ['author'] 消息发起者用户名
    *@return int ['pmtype']  会话类别 1: 私人消息, 2: 群聊消息
    *@return string ['subject']  标题
    *@return int ['members'] 会话参与人数
    *@return int ['dateline']  消息发起时间
    *@return string ['message']  消息内容
    *@return int ['founderuid']  发起会话的用户 ID
    *@return int ['founddateline'] 会话发起时间
    *@return int ['touid'] pmtype参数为1时,该返回值表示对方用户 ID
    *@return int ['msgfromid'][兼容] 发件人用户 ID
    *@return string ['msgfrom'][兼容]  发件人用户名
    *@return int ['msgtoid'][兼容] 收件人用户 ID
    * */
    public function newView(){
      $domain=new Domain_Message;
      $re=$domain->getNews($this->uid); 
      $data=DI()->ucenter->uc_pm_viewnode($re['ucid'],0,$this->pmid);
      return $data;
    }

   /**
   *消息中心
   *@desc 删除消息
   *@return int code 212 删除成功   412 删除失败
   *@return string msg 删除成功 删除失败
   */ 
   
    public function delNew(){

      $domain=new Domain_Message;
      $re=$domain->getNews($this->uid); 
      $pmids=explode(",", $this->pmid);      
      $rt=DI()->ucenter->uc_pm_delete($re['ucid'],null,$pmids);
      if($rt==1){
        foreach ($pmids as $v) {
          $domain->delNew($v);
        }
          $rs['code']=212;
          $rs['msg']="删除成功";
          return $rs ;
      }else{
          $rs['code']=412;
          $rs['msg']="删除失败";
          return $rs ;

      }
    }


}





































































