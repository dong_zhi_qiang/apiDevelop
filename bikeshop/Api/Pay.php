<?php
/**
*支付接口
 * +----------------------------------------------------------------------
 * | 支付
 * +----------------------------------------------------------------------
 * | Copyright (c) 2015 summer All rights reserved.
 * +----------------------------------------------------------------------
 * | Author: summer <aer_c@qq.com> <qq7579476>
 * +----------------------------------------------------------------------
 * | This is not a free software, unauthorized no use and dissemination.
 * +----------------------------------------------------------------------
 * | Date
 * +----------------------------------------------------------------------
 */

class Api_Pay extends PhalApi_Api {

	public function getRules() {
        return array(
            'index' => array(
                'body' 	=> array('name' => 'body', 'type' =>'string', 'require' => true,  'desc' => '商品描述'),
                'price'  => array('name' => 'price', 'type' =>'string', 'require' => true,  'desc' => '总金额'),
                'type'   => array('name' => 'type', 'type' =>'enum', 'require' => true, 'range' => array('aliwap', 'wechat'), 'desc' => '引擎类型，比如aliwap'),
                'order_id'=>array('name'=>'order_id','type'=>'string','require'=>true,'desc'=>'支付号'),
                'title'   =>array('name'=>'title','type'=>'string','require'=>true,'desc'=>'订单标题')
            ),
            'cha'=>array(
              'trade_no'=>array('name' => 'trade_no', 'type' =>'string',   'desc' => '支付宝或微信交易号'),
              'order_id'=>array('name' => 'order_id', 'type' =>'string',   'desc' => '本地订单号（合成后的）'),
              'type'   => array('name' => 'type', 'type' =>'enum', 'require' => true, 'range' => array('aliwap', 'wechat'), 'desc' => '引擎类型，比如aliwap'),
              ),
            'closeOrder'=>[
                 'trade_no'=>array('name' => 'trade_no', 'type' =>'string',   'desc' => '支付宝或微信交易号'),
                 'order_id'=>array('name' => 'order_id', 'type' =>'string',   'desc' => '本地订单号（合成后的）'),
                 'type'   => array('name' => 'type', 'type' =>'enum', 'require' => true, 'range' => array('aliwap', 'wechat'), 'desc' => '引擎类型，比如aliwap'),
            ],
           'refundOrder'=>[
                'trade_no'=>array('name' => 'trade_no', 'type' =>'string', 'require' => true,  'desc' => '支付宝或微信交易号'),
                'order_id'=>array('name' => 'order_id', 'type' =>'string',  'require' => true, 'desc' => '本地订单号（退款单号）'),
                 'type'   => array('name' => 'type', 'type' =>'enum', 'require' => true, 'range' => array('Aliwap', 'Wechat'), 'desc' => '引擎类型，比如aliwap'), 
                 'money'  =>array('name'=>'money','type'=>'string','require' => true,'desc'=>'退款金额'),
                 'reason' =>array('name'=>'reason','type'=>'string','desc'=>'退款原因'),
           ],
        );
	}

	/**
	 * 支付接口
   *@desc统一下单
	 * @return [type] [description]
	 */
	public function index(){
		 //获取对应的支付引擎
        DI()->pay->set($this->type);
        $data = array();
        $data['order_no'] = $this->order_id;
        $data['title']    = $this->title;
        $data['body']     = $this->body;
        $data['price']    = $this->price;
       //DI()->logger->log('paySuccess', 'Pay Success',$data);
       return  DI()->pay->buildRequestForm($data) ;
        exit;
	}

  /**
   * 查询订单
   *@desc查询订单
   * @return boolean status 
   * @return int   type 1定金  0全额
   */
    public function cha(){
       //获取对应的支付引擎
       DI()->pay->set($this->type);
       $domin=new Domain_TheOrder();
        $date=[
           "trade_no"       =>$this->trade_no,
           "out_trade_no"   =>$this->order_id,
        ];
       $data['status']= DI()->pay->orderQuery($date);
       $type=$domin->getType($this->trade_no);
       if($type['pay_name']){
        $data['type']=1; 
      }else{
         $data['type']=0;    
      }       
       return $data;

     }
  /**
   * 关闭订单
   *@desc关闭订单
   * return array
   */
    public function closeOrder(){
        //获取对应的支付引擎
        DI()->pay->set($this->type);
        $date=[
           "trade_no"       =>$this->trade_no,
           "out_trade_no"   =>$this->order_id,
        ];
       $data= DI()->pay->closeOrder($date);
       return $data;
     }
  /**
  *申请退款
  *@desc 申请退款
  *@return  boolean  true  false
  */ 
  public function refundOrder(){
     DI()->pay->set($this->type);
     $domain=new Domain_TheOrder();
     $total_fee=$domain->toolFee($this->trade_no);
     $count=$domain->getCount($this->trade_no);   
    $date=[
           "trade_no"   =>$this->trade_no,
           "money"      =>$this->money,
           "total_fee"  =>$total_fee,
           "out_trade_no"=>"",
          "refund_reason"=>$this->reason, 
        ];
        if($count>1){
          $date["order_id"]=$this->order_id;
        }else{
          $date["order_id"]="";
        }
       $data= DI()->pay->refund($date);
       if($data){
         $datt['status']="-1";
         $domain->upOrder($this->order_id,$datt);
       } 
       return $data;
  }



}