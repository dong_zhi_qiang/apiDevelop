<?php

/**
 * 认证管理
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:02
 */
class Api_Authenticate extends PhalApi_Api
{
    public function getRules()
    {
        return [
            'addShop' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true,  'desc' => '用户id'],
                'name' => ['name' => 'name', 'type' => 'string', 'require' => true,'desc' => '企业名称'],
                'code' => ['name' => 'code', 'type' => 'string', 'require' => true,'desc' => '企业代码'],
                'person' => ['name' => 'person', 'type' => 'string', 'require' => true,'desc' => '企业法人'],
                'payment_account' => ['name' => 'payment_account', 'type' => 'string', 'require' => true,'desc' => '收款账户'],
                'bank' => ['name' => 'bank', 'type' => 'string', 'require' => true, 'desc' => '收款银行'],
                'business_license'=>[
                    'name' => 'business_license', 'type' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'),
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'营业执照'
                ],
            ],
            'shopAuthInfo'=>[
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户id'],
            ],
        ];
    }

    /**
     * 上传商家认证资料
     * @desc 上传商家认证资料
     * @return int id 1表示添加成功.0表示失败
     */
    public function addShop()
    {
        $authenticate = new Domain_Authenticate();

        $dst = DI()->helper->getFileName('shopAuth',$this->business_license); //获取logo的上传路径

        $license= DI()->cosSdk->uploadCos('bikeshop', $this->business_license['tmp_name'], $dst);

        $licenseData = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com",$license['data']['source_url']);//替换域名

        $data = [
            'uid' => $this->uid,
            'name' => $this->name,
            'code' => $this->code,
            'person' => $this->person,
            'payment_account' => $this->payment_account,
            'bank' => $this->bank,
            'business_license' => $licenseData,
            'time'=>date('Y-m-d H:i:s')
        ];

        $rs = $authenticate->addShop($data);

        return $rs ? true :false;
    }


    /**
     * 获取商家认证资料信息
     * @desc 根据uid,获取商家认证资料信息
     * @return mixed
     */
    public function shopAuthInfo(){

        $authenticate = new Domain_Authenticate();
        $field = 'name,code,person,payment_account,bank';
        $arg = [
          'uid' => $this->uid
        ];

        return $authenticate->getFieldByData($field,$arg);
    }



}