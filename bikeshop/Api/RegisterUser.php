<?php

/**
 * 注册管理 -
 * Date: 2016/9/22
 * Time: 15:30
 */
class Api_RegisterUser extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'perfectInfo' => [
                'username' => ['name' => 'username', 'type' => 'string', 'require' => true, 'desc' => '用户名'],
                 'phone' => ['name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号'],
                'password' => ['name' => 'password', 'type' => 'string', 'require' => true, 'desc' => '用户密码'],
                'email' => ['name' => 'email', 'type' => 'string', 'require' => true, 'desc' => '用户邮箱'],
            ],
            'addUser'      => [
              'phone' => ['name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号'],
              'code'  => ['name' => 'code', 'type' => 'string', 'require' => true, 'desc' => '验证码'],
            ],  
            'registerUser' => [
                'username' => ['name' => 'username', 'type' => 'string', 'require' => true, 'desc' => '用户名'],
            ],
            'registerPhone' => [
                'phone' => ['name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号'],
            ],
            'registerEmail' => [
                'email' => ['name' => 'email', 'type' => 'string', 'require' => true, 'desc' => '用户邮箱'],
            ],
            'authCode' => [
                'phone' => ['name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号'],
                 'sms'=>['name' => 'sms', 'type' => 'string', 'default'=>"SMS_16220588", 'desc' => 'SMS_16220588->用户注册/SMS_16220585->信息变更/SMS_16220586->修改密码'],
            ],
            
            'updatePwd'=>[
                  'phone' => ['name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号'],
                  'password' => ['name' => 'password', 'type' => 'string', 'require' => true, 'desc' => '用户密码'],
            ],
        ];
    }


    /**
     *注册用户信息(2.0 修改)
     * @desc 注册用户信息
     * @return int code 操作码，201表示验证通过，   401手机号已注册  403验证码错误或已失效
     * @return string msg 操作码  手机号已注册    验证通过
     *
     */
    public function addUser()
    {
        $phone =$this->phone;
        $code=$this->code;
        $domain = new Domain_RegisterUser();
        $data = [ 
            'phone' => $phone,
        ];
        $info = $domain->user('phone', $phone);
        if($info){
            $rs['code'] = 401;
            $rs['msg'] = T('该手机号已注册');
            return $rs;
            die;
        } 
        $codeTime=$domain->VerificationCode($phone,$code);
        if($codeTime==false || date("Y-m-d H:i:s")>$codeTime['time']  ){
            $rs['code'] = 403;
            $rs['msg'] = T('验证码错误或已失效');
            return $rs;
        }else{
            $rs['code'] = 201;
            $rs['msg'] =T("验证通过");
            return $rs;
        }
    }

    /**
     *完善用户资料(2.0)
     *@desc完善用户资料
     * @return int code 操作码，201表示注册成功，  405用户名已注册   402 邮箱已注册 406 具体看返回信息
     * @return string msg 操作码  手机号已注册 邮箱已注册   返回一个数字时表示注册成功  注册失败 
     */
    public function perfectInfo(){
        $username = $this->username;
        $password = $this->password;
        $email = $this->email;
        $domain = new Domain_RegisterUser();
        if($domain->user('username', $username)){
            $rs['code'] = 405;
            $rs['msg'] = T('用户名已注册');
            return $rs;
        }
        if($domain->user('email', $email)){
            $rs['code'] = 402;
            $rs['msg'] = T('该邮箱已注册');
            return $rs;
        } 
         $data = [
            'phone'    =>$this->phone,
            'username' => $username,
            'password' =>DI()->md5->md5_encode($password),
            'email' => $email,
        ];
        $re = DI()->ucenter->register($username, $password, $email);
        if (is_int($re)) {
            $data['ucid']=$re;
            $uid=$domain->inBaseUser($data);
            $rs['code'] = 201;
            $rs['msg'] =$uid;
            return $rs;
        }else {
            $rs['code'] = 406;
            $rs['msg'] = T($re);
            return $rs;
        }
    }
    /**
     *注册验证
     * @desc 验证用户名
     * @return int code 操作码，401 已注册  未注册
     * @return string msg 操作码  用户名已注册  用户名未注册
     *
     */

    public function  registerUser()
    {
        $rs = array();
        $username = $_POST['username'];
        $domain = new Domain_RegisterUser();
        $info = $domain->user('username', $username);
        if ($info) {
            $rs['code'] = 401;
            $rs['msg'] = T('该用户名已注册');
            return $rs;
        } else {
            $rs['code'] = 201;
            $rs['msg'] = T('该用户名未注册');
            return $rs;
        }
    }

    /**
     *注册验证
     * @desc 验证手机号
     * @return int code 操作码，401 已注册  201未注册
     * @return string msg 操作码  手机号已注册  手机号未注册
     *
     */

    public function  registerPhone()
    {
        $rs = array();
        $phone = $_POST['phone'];
        $domain = new Domain_RegisterUser();
        $info = $domain->user('phone', $phone);
        if ($info) {
            $rs['code'] = 401;
            $rs['msg'] = T('该手机号已注册');
            return $rs;
        } else {
            $rs['code'] = 201;
            $rs['msg'] = T('该手机号未注册');
            return $rs;
        }
    }

    /**
     *注册验证
     * @desc 验证邮箱
     * @return int code 操作码，401 已注册  未注册
     * @return string msg 操作码  邮箱已注册  该邮箱未注册
     *
     */

    public function  registerEmail()
    {
        $rs = array();
        $email = $_POST['email'];
        $domain = new Domain_RegisterUser();
        $info = $domain->user('email', $email);
        if ($info) {
            $rs['code'] = 401;
            $rs['msg'] = T('该邮箱已注册');
            return $rs;
        } else {
            $rs['code'] = 201;
            $rs['msg'] = T('该邮箱未注册');
            return $rs;
        }
    }

    /**
     *获取验证码
     * @desc 获取验证码
     * @return int code 操作码，204 发送成功  407发送失败
     * @return string msg 操作码  发送成功
     *
     */
    public function  authCode()
    {
        $phone = $this->phone;
        $rs = array();
        $code = rand(100000, 999999);
        $re=DI()->note->register($phone, $code,"憬骑","单车易购", "SMS_16220588");
        $data=array(
          'phone'=>$phone,
          'code' =>$code,
          'time' =>date("Y-m-d H:i:s",strtotime("+3 minute")),
          );
         $domain = new Domain_RegisterUser();
         $re=$domain->inCode($data);
        $rs['code'] = 204;
        $rs['msg'] = T("发送成功");
        $rs['data'] = T($code);
        return $rs;


    }
 
 /**
  *找回密码
  *@desc 找回密码
  *@return int code 操作码 205 修改成功  405 修改失败
  *@return string msg 操作码 修改成功  修改失败
  */ 
 public function updatePwd(){
    $phone=$this->phone;
    $password=$this->password;
    $rs=array();
    $domain = new Domain_RegisterUser(); 
    $re=$domain->getPwd($phone);
    if($re){
      $rt = DI()->ucenter->uc_user_edit($re['username'],$re['password'],$password,$re['email'],[1]);
      $domain->upPwd($phone,DI()->md5->md5_encode($password));
      $rs['code'] = 205;
      $rs['msg'] = T($rt);
      return $rs; 
    }else{
      $rs['code'] = 405;
      $rs['msg'] = T("修改失败"); 
     return $rs;
    }
  }


}





































































