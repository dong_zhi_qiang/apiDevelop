<?php

/**
 * 个人中心 -
 * Date: 2016/9/27
 * Time: 11:30
 */
class Api_UserCenter extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'getUser' => [
                'uid'=>['name'=>'uid','type'=>'string','require'=>true,'desc'=>'用户ID' ],
            ],
            'updatePwd'=>[
                 'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
                 'oldPwd' => ['name' => 'oldPwd', 'type' => 'string', 'require' => true, 'desc' => '用户旧密码'],
                 'newPwd' => ['name' => 'newPwd', 'type' => 'string', 'require' => true, 'desc' => '用户新密码'],
            ],
           'updateName'=>[
                'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
                'username' => ['name' => 'username', 'type' => 'string', 'require' => true, 'desc' => '个人帐号'],
           ], 
          'uploadPic'=>[
             'file'=>[
                'name' => 'file', 'type' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'用户图像'      
                      ],
               'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
          ], 
          'upPhone'=>[
                'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
                'phone' => ['name' => 'phone', 'type' => 'int', 'require' => true, 'desc' => '用户手机号'],
          ],
          'Authenticate'=>[
                 'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
                 'name'   => ['name' => 'name', 'type' => 'string', 'require' => true, 'desc' => '姓名'],
                 'type'   => ['name' => 'type', 'type' => 'string', 'require' => true, 'desc' => '证件类型'],
                 'address'=>['name' => 'address', 'type' => 'string', 'require' => true, 'desc' => '证件地址'],
                 'tel'    =>['name' => 'tel', 'type' => 'string', 'require' => true, 'desc' => '联系电话'],
                 'number' =>['name' => 'number', 'type' => 'string', 'require' => true, 'desc' => '证件号'],
              'file1'=>[
                   'name' => 'file1', 'type' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'证件照正面'      
                      ],
              'file2'=>[
                   'name' => 'file2', 'type' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'证件照背面'      
                      ], 
               'file3'=>[
                   'name' => 'file3', 'type' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'手持证件照'      
                      ],
                  ],
             'getAuthenticate'=>[
              'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],   
                ],

              'addAuthenticate'=>[
                 'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],
                 'name'   => ['name' => 'name', 'type' => 'string', 'require' => true, 'desc' => '姓名'],
                 'address'=>['name' => 'address', 'type' => 'string', 'require' => true, 'desc' => '证件地址'],
                 'url'    =>['name' => 'url', 'type' => 'string', 'require' => true, 'desc' => '证件url'],
                 'number' =>['name' => 'number', 'type' => 'string', 'require' => true, 'desc' => '证件号'],
               ],
              'isAuthenticate'=>[
                'uid'=>['name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID' ],   
                ],  
             'getOrc'=>[
                'image'=>  ['name' => 'image', 'type' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'证件照'  ]    
             ],
            'test'=>[
             'ver'=>['name'=>'ver','type'=>'string','require'=>true,'desc'=>'版本号' ],   
            ],
            'upDowload'=>[
              'eid'=>['name'=>'eid','type'=>'string','require'=>true,'desc'=>'版本ID' ],   
            ], 
        ];
    }


    /**
     *用户信息
     * @desc 用户信息
     * @return int uid               用户Id
     * @return string  username      用户名
     * @return string  pic           用户图像 为空时 使用默认图像
     * @return string  phone         用户手机号
     * @return string  email         用户邮箱
     * @return array   enterprise_certification         -1未认证 0审核中 1认证成功  2认证失败
     * @return array   my_certification           -1未认证 0审核中 1认证成功  2认证失败
     * @return string  shop_num  门店数量 0没有 其他都是有
     */
    public function getUser()
    {
        $username= $this->uid;
        $domain = new Domain_UserCenter();
        $data = $domain->user($username);
           $shop=$domain->getShop($this->uid);
           $shopnum=$domain->getShopNum($this->uid);
           if($shop){
             $data['enterprise_certification']=$shop['status'];
           }else{
             $data['enterprise_certification']='-1';
           }
           $me=$domain->getAuthenticate($this->uid);
           if($me){
            $data['my_certification']=$me['status'];
           }else{
             $data['my_certification']='-1';
           }
           $data['shop_num']=$shopnum;
        return $data;
    }
  
 /**
  *修改密码
  *@desc 修改密码
  *@return int code 操作码 205 修改成功  405 修改失败 505旧密码错误
  *@return string msg 操作码 修改成功  修改失败  旧密码错误
  */ 

   public function updatePwd(){
       $uid=$this->uid;
       $oldPwd=$this->oldPwd;
       $newPwd=$this->newPwd;
       $rs=array();
       $domain = new Domain_UserCenter();
       $re = $domain->getPwd($uid);
       if($re['password']==DI()->md5->md5_encode($oldPwd)){
        if ($domain->upPwd($uid,DI()->md5->md5_encode($newPwd))){
           $rt = DI()->ucenter->uc_user_edit($re['username'],$oldPwd,$newPwd,$re['email'],1);
           $rs['code'] = 205;
           $rs['msg'] = T($rt);
           return $rs;
        }else{
          $rs['code'] = 405;
          $rs['msg'] = T("修改失败");
          return $rs;  
        }
      }else{
        $rs['code'] = 505;
        $rs['msg'] = T("旧密码错误");
        return $rs;  
     } 
   } 
 
 /**
  *修改用户名
  *@desc 修改用户名
  *@return int code 操作码 205 修改成功  405修改失败 501该用户名已被使用
  *@return string msg 操作码 修改成功  修改失败   该用户名已被使用
  */ 

   public function updateName(){
    header("Content-type:text/html;charset=utf-8");
       $uid=$this->uid;
       $username=$this->username;
       $rs=array();
       $domain = new Domain_UserCenter();
       if($domain->getName($username)){
          $rs['code'] = 501;
          $rs['msg'] = T("该用户名已被使用");
          return $rs;  
      }else{               
           $ret = $domain->getPwd($uid);
           //DI()->ucenter->uc_user_delete($ret['ucid']);
           $rt= DI()->ucenter->uc_user_merge($ret['username'],$username,$ret['ucid'],DI()->md5->md5_decode($ret['password']),$ret['email']);         
          if(is_int($rt)){
             $domain->upName($uid,$username);
             $rs['code'] = 205;
             $rs['msg'] = T("修改成功");
             return $rs;
          }else{
             $rs['code'] = 405;
             $rs['msg'] = T($rt);
             return $rs;
          }
       } 
   }
 
  /**
  *修改用户图像
  *@desc 修改用户图像
  *@return int code 操作码 209 修改成功  409修改失败 
  *@return string msg 操作码 修改成功  修改失败   
  *
  */
   public function uploadPic(){
        $uid=$this->uid;
        $domain = new Domain_UserCenter();
        $re= $domain->getPic($uid);  
        $res= DI()->ucenter->uc_avatar($re['ucid'],$this->file);//修改图片
      if($res){
           $image_url=DI()->ucenter->uc_image_url($re['ucid'],'middle');
          $domain->upload($uid,$image_url);
          $rs['code']=209;
          $rs['msg'] = T("修改成功");
          return $rs;       
      }else{
          $rs['code']=409;
          $rs['msg'] = T("修改失败");
          return $rs; 
     } 
   }
 

    /**
  *修改手机号
  *@desc 修改手机号
  *@return int code 操作码 209 修改成功  409修改失败 
  *@return string msg 操作码 修改成功  修改失败   
  *
  */

  public function upPhone(){
    $rs=array();
    $domain = new Domain_UserCenter();
    if($domain->upPhone($this->uid,$this->phone)){
          $rs['code']=209;
          $rs['msg'] = T("修改成功");
          return $rs; 
        }else{
          $rs['code']=409;
          $rs['msg'] = T("修改失败");
          return $rs; 
        }  
    }

 /**
 * 个人中心
 *@desc 实名验证
 *@return int code 操作码 209 提交成功  409提交失败 
 *@return string msg 操作码 提交成功  提交失败  
 */
    public function  Authenticate(){
       $rs=array();

        $dst = DI()->helper->getFileName('userAuth', $this->file1); //获取logo的上传路径
        $dst2 = DI()->helper->getFileName('userAuth', $this->file2); //获取image_url的上传路径
        $dst3 = DI()->helper->getFileName('userAuth', $this->file3); //获取image_url的上传路径

        $file1 = DI()->cosSdk->uploadCos('bikeshop', $this->file1['tmp_name'], $dst);
        $file2 = DI()->cosSdk->uploadCos('bikeshop', $this->file2['tmp_name'], $dst2);
        $file3 = DI()->cosSdk->uploadCos('bikeshop', $this->file3['tmp_name'], $dst3);

        $file1Score = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $file1['data']['source_url']);
        $file2Score = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com",$file2['data']['source_url']);
        $file3Score = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $file3['data']['source_url']);

        $data = [
            'uid'    =>$this->uid,
            'name'   =>$this->name,
            'type'   =>$this->type,
            'number' =>$this->number,
            'address' =>$this->address,
            'tel'     =>$this->tel,
            'positive'=>$file1Score,
            'negative'=>$file2Score,
            'handheld'=>$file3Score,
         ];

        $domain = new Domain_UserCenter();
        if($domain->inAuthenticate($data)){
           $rs['code']=209;
           $rs['msg'] = T("提交成功");
           return $rs; 
        }else{
          $rs['code']=409;
          $rs['msg'] = T("提交失败");
          return $rs; 
        }        
      }

     /** 
     *个人中心
     *@desc  实名信息
     *@return int   uid  用户ID
     *@return string name 姓名
     *@return string tel  联系方式
     *@return string address地址 
     *@return string  positive 证件照正面
     *@return string  negative  反面 
     *@return string  handheld  手持面
     */
    public function   getAuthenticate(){
      $domain = new Domain_UserCenter();
      $data=$domain->Authenticate($this->uid);
      return $data;
    }

   /**
   *个人中心
   *@desc是否认证
   *@return string   me   -1未认证 0审核中 1认证成功  2认证失败
   */

  public  function isAuthenticate(){

    $domain = new Domain_UserCenter();
    $me=$domain->getAuthenticate($this->uid);
           if($me){
            $me=$me['status'];
           }else{
             $me='-1';
           }
     return $me;       
  }

 /**
 *身份证OCR-图片URL
 * @desc身份识别 
 *@return int code 405不是身份证，201是身份证 407 获取失败
 */
 public  function getOrc(){

    $dst = DI()->helper->getFileName('userAuth', $this->image); //获取image_url的上传路径

    $file = DI()->cosSdk->uploadCos('bikeshop', $this->image['tmp_name'], $dst);
     
    $url = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $file['data']['source_url']);

    $tir=DI()->ocr->Obtain([$url]);
    if(isset($tir->result_list)){
        $re=$tir->result_list;
       if($re[0]->code!=0){
         $rs['code']=405;
         $rs['msg']=T('图片错误');
         return $rs; 
      }else{
         $rs['code']=201;
         $rs['msg']=T('图片正确');
         $data=[
          'url'    =>$re[0]->url,
          'name'   =>$re[0]->data->name,
          'address'=>$re[0]->data->address,
          'user_id'=>$re[0]->data->id,
         ];
         $rs['info']=$data;
         return $rs;
       }
    }else{
      $rs['code']=407;
      $rs['msg']=T('获取失败');
      return $rs;
    }
  }
  /**
 * 个人中心
 *@desc 身份证实名验证
 *@return int code 操作码 209 提交成功  409提交失败 
 *@return string msg 操作码 提交成功  提交失败  
 */
 public  function addAuthenticate(){
   
    $data = [
            'uid'    =>$this->uid,
            'name'   =>$this->name,
            'type'   =>"身份证",
            'number' =>$this->number,
            'address' =>$this->address,
            'positive'=>$this->url,
            'status'  =>1,
         ];

        $domain = new Domain_UserCenter();
        if($domain->inAuthenticate($data)){
           $rs['code']=209;
           $rs['msg'] = T("提交成功");
           return $rs; 
        }else{
          $rs['code']=409;
          $rs['msg'] = T("提交失败");
          return $rs; 
        }        
      }
  /**
   *版本控制
   * @desc 版本控制
   *@return sting version  版本号
   */
  public function edition(){
   
   $domain=new Domain_UserCenter();
   $date=$domain->getEdition();
   $coun=$domain->userNum();
   $date['num']=ceil(($date['num']/$coun)*100).'%';
   return $date;
  }
 /**
   *检测版本
   * @desc 检测版本
   *@return boolean true 不是最新版本 false 是最新版本 
   */ 
 public  function test(){
  
    $domain=new Domain_UserCenter();  
    $re=$domain->testEdition($this->ver);
    if($re){
      return true;
    }else{
      return false;
     }
  }
 /**
  *更新下载数量
  *@desc 更新下载数量
  *@return int 1  0
  */
  public function upDowload(){

    $domain=new Domain_UserCenter();
    $date=[
      'num'=>new NotORM_Literal("num +1"),
    ];
   return $domain->updowloadNUm($this->eid,$date);
  } 
}

































































