<?php
/**
 * 请在下面放置任何您需要的应用配置
 */

return array(

    /**
     * 应用接口层的统一参数
     */
    'apiCommonRules' => array(
        //'sign' => array('name' => 'sign', 'require' => true),
    ),
   'UCloudEngine' => 'local',

  'UCloud' => array(
        //对应的文件路径
        'host' => "http://{$_SERVER["SERVER_NAME"]}/shopapi/upload" 
    ),
  'Md5'=>array(
     'accessKey'=>"accessKey",
      'Key'=>"Key",
    ),
   //微信支付设置
        // 'Weixin' => array(
        //     //公众号的唯一标识
        //     'appid' => 'wx6f5123fa8506f28e',

        //     //商户号
        //     'mchid' => '1395816802',

        //     //公众号的appsecret
        //    // 'appsecret' => 'eeedb962aefc8ac9ab93702de430fd8b',

        //    'notify_url'=> "http://192.168.1.119/bikeshop/V1/result.php",

        //     //微信支付Key
        //     'key' => 'qwertyuiopasdfghjkl123456zxcvbnm',
        // ),


   /**
     * 支付相关配置
     */
    'Pay' => array(
        //异步/同步地址
        'notify_url' => "http://{$_SERVER["SERVER_NAME"]}/V1/pay/",

        //支付宝wap端设置
        'aliwap' => array( 
            //收款账号邮箱
            'email' => 'email',

            //加密key
            'key' => 'key',

            //合作者ID
            'partner' => 'partner'
        ),

        //微信支付设置
        'wechat' => array(
            //公众号的唯一标识
            'appid' => 'appid',

            //商户号
            'mchid' => 'mchid',

            //微信支付Key
            'key' => 'key',
        ),
    ),

    /**
     * 高德key
     */
    'geocode' => array(
        //对应的文件路径
        'key' => 'key'
    )
);
