<?php
/**
 * 分库分表的自定义数据库路由配置
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2015-02-09
 */
return [


    /**
     * 单车易购测试数据库
     */
    'bikeshop' => [
        'debug' => true,
        'servers' => [
            'bikeshop' => [                         //服务器标记
                'host' => '127.0.0.1',             //数据库域名
                'name' => 'bikeshop',               //数据库名字
                'user' => 'jingqi',                  //数据库用户名
                'password' => '883838Qedd',                        //数据库密码
                'port' => '4450',                  //数据库端口
                'charset' => 'UTF8',                  //数据库字符集
            ],
        ],

        /**
         * 自定义路由表
         */
        'tables' => [
            //通用路由
            '__default__' => [
                'prefix' => '',
                'key' => 'id',
                'map' => [
                    ['db' => 'bikeshop'],
                ],
            ],
        ],
    ],

    /**
     * 自行车在线测试数据库
     */
    'ucwei' => [
        'debug' => true,
        'servers' => [
            'zixc' => [                                                //服务器标记
                'host' => '127.0.0.1',             //数据库域名
                'name' => 'zixc',               //数据库名字
                'user' => 'jingqi',                  //数据库用户名
                'password' => '883838Qedd',          //数据库密码
                'port' => '4450',                  //数据库端口
                'charset' => 'UTF8',                  //数据库字符集
            ],
        ],

        /**
         * 自定义路由表
         */
        'tables' => [
            //通用路由
            '__default__' => [
                'prefix' => '',
                'key' => 'id',
                'map' => [
                    ['db' => 'zixc'],
                ],
            ],
        ],
    ],


    //测试discuz数据库
    'test' => [
        'debug' => true,
        'servers' => [
            'zixc' => [                         //服务器标记
                'host' => '127.0.0.1',             //数据库域名
                'name' => 'zixc',               //数据库名字
                'user' => 'jingqi',                  //数据库用户名
                'password' => '883838Qedd',                        //数据库密码
                'port' => '4450',                  //数据库端口
                'charset' => 'UTF8',                  //数据库字符集
            ],
        ],

        /**
         * 自定义路由表
         */
        'tables' => [
            //通用路由
            '__default__' => [
                'prefix' => '',
                'key' => 'id',
                'map' => [
                    ['db' => 'zixc'],
                ],
            ],
        ],
    ],

    //显示discuz数据库
    'olbike' => [
        'debug' => true,
        'servers' => [
            'olbike' => [                         //服务器标记
                'host' => '127.0.0.1',             //数据库域名
                'name' => 'zixc',               //数据库名字
                'user' => 'bikeshopapi',                  //数据库用户名
                'password' => '123456789QQ',                        //数据库密码
                'port' => '4729',                  //数据库端口
                'charset' => 'UTF8',                  //数据库字符集
            ],
        ],

        /**
         * 自定义路由表
         */
        'tables' => [
            //通用路由
            '__default__' => [
                'prefix' => '',
                'key' => 'id',
                'map' => [
                    ['db' => 'online_bike'],
                ],
            ],
        ],
    ],

];
