<?php

/**
 * 直播管理
 * Date: 2016/9/22
 * Time: 15:30
 */
class Api_Live extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'add' => [
                'channelName' => ['name' => 'channelName', 'type' => 'string', 'require' => true, 'desc' => '直播频道的名称'],
                'outputSourceType' => ['name' => 'outputSourceType', 'type' => 'int', 'require' => true, 'desc' => '选择输出源类型(输出3有RTMP/FLV HLS)'],
                'channelDescribe' => ['name' => 'channelDescribe', 'type' => 'string', 'require' => true, 'desc' => '直播频道描述'],
                'sourceName' => ['name' => 'sourceName', 'type' => 'string', 'require' => true, 'desc' => '直播源名称'],
                'sourceType' => ['name' => 'sourceType', 'type' => 'int', 'require' => true, 'desc' => '1 rtmp推流'],
                'outputRate' => ['name' => 'outputRate', 'type' => 'array', 'format' => 'explode', 'require' => true,'desc' => '原画：0 标清：10 高清：20::0必传', 'separator' => ','],
            ],
            'getList' => [
                'channelStatus' => ['name' => 'channelStatus', 'type' => 'string', 'require' => true, 'default' => '0', 'desc' => '用作频道列表过滤，频道状态（0：无输入流;1：有输入流;2：异常;3：关闭;4：配置不完整）'],
                'pageNo' => ['name' => 'pageNo', 'type' => 'string', 'require' => false, 'default' => '3', 'desc' => '分页页号，比如想查看第3页的列表，将该变量设置为3'],
                'pageSize' => ['name' => 'pageSize', 'type' => 'string', 'require' => false, 'default' => '2', 'desc' => '每一个分页上显示的频道的个数'],
            ],
            'getListDetails' => [
                'channelId' => ['name' => 'channelId', 'type' => 'string', 'require' => true, 'desc' => '频道的ID号']
            ],
            'edit' => [
                'channelId' => ['name' => 'channelId', 'type' => 'string', 'require' => true, 'desc' => '频道的ID号'],
                'channelName' => ['name' => 'channelName', 'type' => 'string', 'require' => true, 'desc' => '直播频道名称'],
                'channelDescribe' => ['name' => 'channelDescribe', 'type' => 'string', 'require' => false, 'desc' => '直播频道描述']
            ],
            'delete' => [
                'channelIds' => ['name' => 'channelIds', 'type' => 'array', 'format' => 'explode', 'require' => true, 'separator' => ',', 'desc' => '频道的ID值，支持批量,数组'],
            ],
            'start' => [
                'channelIds' => ['name' => 'channelIds', 'type' => 'string', 'require' => true, 'desc' => '频道的ID值'],

//                'channelIds' => ['name' => 'channelIds', 'type' => 'array', 'format' => 'explode', 'require' => true, 'separator' => ',', 'desc' => '频道的ID值，支持批量,数组'],
            ],
            'stop' => [
                'channelIds' => ['name' => 'channelIds', 'type' => 'string', 'require' => true, 'desc' => '频道的ID值'],

//                'channelIds' => ['name' => 'channelIds', 'type' => 'array', 'format' => 'explode', 'require' => true, 'separator' => ',', 'desc' => '频道的ID值，支持批量,数组'],
            ],
        ];
    }

    const MODULE = 'live';

    /**
     * 创建直播频道
     * @desc 创建直播频道
     * @return string url 要请求的url地址
     * @return Int code    错误码,0: 成功, 其他值: 失败
     * @return String message 错误信息
     * @return String codeDesc Success表示成功
     * @return Int Channel_id 创建频道的ID
     * @return array downstream_address 输出频道的信息
     */
    public function add()
    {
        $interface = 'CreateLVBChannel';
        $package = [
            'channelName' => $this->channelName,
            'outputSourceType' => $this->outputSourceType,
            'channelDescribe' => $this->channelDescribe,
            'sourceList.1.name' => $this->sourceName,
            'sourceList.1.type' => $this->sourceType,
            'outputRate.1' => $this->outputRate[0],
            'outputRate.2' => $this->outputRate[1],
        ];


        $url = DI()->qcloud->load(self::MODULE, $interface, $package);

        $result = json_decode(file_get_contents($url),true);


        return [
            'code' => $result['code'],
            'message'=>$result['message'],
            'codeDesc' => $result['codeDesc'],
            'channel_id' => $result['channel_id'],
            'downstream_address'=>$result['channelInfo']['downstream_address']
        ];
    }


    /**
     * 查询直播列表
     * @desc 获取用户的全部直播频道信息，包括频道的ID、当前状态、名称和创建时间。
     * @return Int code 错误码,0: 成功, 其他值: 失败
     * @return String message 错误信息
     * @return Int all_count 总频道数
     * @return Int channel_id 频道id
     * @return array channelSet 频道列表
     * @return string channel_name 频道名称
     * @return Int channel_status 频道状态码
     * @return string create_time 创建时间
     */
    public function getList()
    {
        $interface = 'DescribeLVBChannelList';

        $package = [
            'channelStatus' => $this->channelStatus,
//            'pageNo' => $this->pageNo,
//            'pageSize' => $this-> pageSize,
        ];
        $url = DI()->qcloud->load(self::MODULE, $interface, $package);

        $request = json_decode(file_get_contents($url), true);

        return $request['channelSet'];
    }


    /**
     * 查询直播频道详情
     * @desc 输入待查询频道的ID号，获得该频道的当前状态、名称、描述、直播源信息和输出源信息。
     * @return Int code    错误码, 0: 成功, 其他值: 失败
     * @return String message    错误信息
     * @return Int channel_id 频道id
     * @return string channel_describe 频道描述
     * @return string channel_name 频道名称
     * @return Int channel_status 频道状态码
     * @return array upstream_list    直播源数据 当前状态、名称、描述、直播源信息和输出源信息
     * @return array upstream_list['sourceName'] 直播源名称
     * @return array upstream_list['sourceID'] 直播源ID
     * @return array upstream_list['sourceType'] 协议类型1:RTMP推流  2    :RTMP拉流 3:HLS拉流
     * @return array upstream_list['sourceAddress'] 地址
     * @return string channel_describe 频道描述
     * @return string resolution 分辨率
     * @return string password 播放密码
     * @return string rtmp_downstream_address 播放地址(RTMP)
     * @return string flv_downstream_address 播放地址(FLV)
     */
    public function getListDetails()
    {
        $interface = 'DescribeLVBChannel';
        $package = [
            'channelId' => $this->channelId,
        ];

        $url = DI()->qcloud->load(self::MODULE, $interface, $package);

        $request = json_decode(file_get_contents($url), true);

        return $request['channelInfo'];
    }


    /**
     * 修改直播频道信息
     * @desc 修改直播频道的基本信息，包括频道的名称和描述信息。
     * @return Int code    错误码, 0: 成功, 其他值: 失败
     * @return String message    错误信息
     */
    public function edit()
    {
        $interface = 'ModifyLVBChannel';

        if($this->channelDescribe){
            $package = [
                'channelId' => $this->channelId,
                'channelName' => $this->channelName,
                'channelDescribe' => $this->channelDescribe,
            ];
        }else{
            $package = [
                'channelId' => $this->channelId,
                'channelName' => $this->channelName,
            ];
        }

        $url = DI()->qcloud->load(self::MODULE, $interface, $package);

        $request = json_decode(file_get_contents($url),true);

        return [
            'code' => $request['code'],
            'message' =>$request['message'],
        ];

    }


    /**
     * 删除直播频道
     * @desc 输入待删除的直播频道的ID号（支持批量），删除该频道。
     * @return Int code    错误码, 0: 成功, 其他值: 失败
     * @return String message    错误信息
     */
    public function delete()
    {
        $interface = 'DeleteLVBChannel';

        $package = [
            'channelIds.1' => $this->channelIds,
        ];

        $url = DI()->qcloud->load(self::MODULE, $interface, $package);

        $request = json_decode(file_get_contents($url),true);

        return [
            'code' => $request['code'],
            'message' =>$request['message'],
        ];

    }


    /**
     * 批量启用直播频道
     * @desc 输入待启用直播频道的ID号（支持批量），启动这些直播频道。
     * @return Int code    错误码, 0: 成功, 其他值: 失败
     * @return String message    错误信息
     */
    public function start()
    {
        $interface = 'StartLVBChannel';
        $package = [
            'channelIds.1' => $this->channelIds,
        ];

        $url = DI()->qcloud->load(self::MODULE, $interface, $package);

        $request = json_decode(file_get_contents($url),true);

        return [
            'code' => $request['code'],
            'message' =>$request['message'],
        ];
    }


    /**
     * 批量停止直播频道
     * @desc 输入待停止直播频道的ID号（支持批量），停止这些直播频道。
     * @return Int code    错误码, 0: 成功, 其他值: 失败
     * @return String message    错误信息
     */
    public function stop()
    {
        $interface = 'StopLVBChannel';
        $package = [
            'channelIds.1' => $this->channelIds,
        ];

        $url = DI()->qcloud->load('live', $interface, $package);


        $request = json_decode(file_get_contents($url),true);

        return [
            'code' => $request['code'],
            'message' =>$request['message'],
        ];
    }
}










