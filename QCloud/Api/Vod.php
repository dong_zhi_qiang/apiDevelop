<?php
/**
 * 点播管理
 * Date: 2016/12/26
 * Time: 13:43:13
 */
class Api_Vod extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'vodPlayInfo' => [
                'pageNo' => ['name' => 'pageNo', 'type' => 'int', 'require' => false, 'desc' => '分页页号'],
                'pageSize' => ['name' => 'pageSize', 'type' => 'int', 'require' => false, 'desc' => '分页大小，范围在10-100之间'],
                'status' => ['name' => 'status', 'type' => 'int', 'require' => false, 'desc' => '视频状态：-1：未上传完成，不存在；0：初始化，暂未使用；1：审核不通过，暂未使用；2：正常；3：暂停；4：转码中；5：发布中；6：删除中；7：转码失败；10：转码等待中; 11: 部分转码成功；100：已删除'],
                'orderby' => ['name' => 'orderby', 'type' => 'int', 'require' => false, 'desc' => '结果排序，默认按时间降序，0：按时间升序 1：按时间降序'],
            ]
        ];
    }

    const MODULE = 'vod';

    /**
     * 获取视频播放信息列表
     * @desc 获取视频信息，可以根据视频文件名获得视频信息列表。
     * @return int code 错误码, 0: 成功, 其他值: 失败
     * @return string message 错误信息
     */
    public function vodPlayInfo(){

        $interface = 'DescribeVodInfo';
        $package = [
//fileIds.n	否	String	视频ID列表，暂时不支持批量
//from	否	String	开始时间，默认为1970-1-1 00:00:00
//to	否	String	结束时间，默认为 2038-1-1 00:00:00
//classId	否	Int	视频分类ID，过滤使用
//status	否	Int	视频状态，过滤使用， -1：未上传完成，不存在；0：初始化，暂未使用；1：审核不通过，暂未使用；2：正常；3：暂停；4：转码中；5：发布中；6：删除中；7：转码失败；10：转码等待中; 11: 部分转码成功；100：已删除
//orderby	否	Int	结果排序，默认按时间降序，0：按时间升序 1：按时间降序
//pageNo	否	Int	分页页号
//pageSize	否	Int	分页大小，范围在10-100之间
//            'pageNo'=> $this->pageNo ? $this->pageNo : 1,
//            'pageSize'=> $this->pageSize ? $this->pageSize : 12,
//            'status'=> $this->status ? $this->status : 2,
            'orderby'=> $this->orderby,
        ];
//        return $package;

        $url = DI()->qcloud->load(self::MODULE, $interface,$package);

        $result = json_decode(file_get_contents($url),true);

        return $result;

//        $interface = 'DescribeVodPlayInfo';
//        $package = [
//            'fileName' => $this->fileName ? 'live'.$this->fileName : 'live',
//            'pageNo' => $this->pageNo ? $this->pageNo : 1,
//            'pageSize' => $this->pageSize ? $this->pageSize : 12,
//        ];
////        return $package;
//
//        $url = DI()->qcloud->load(self::MODULE, $interface,$package);
//
//        $result = json_decode(file_get_contents($url),true);
//
//        return $result;

    }




}










